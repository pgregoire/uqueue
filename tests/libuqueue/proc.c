/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <string.h>

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */

static int uqctl(int uq, int pid, int add)
{
	struct uevent ue;

	add = (0 != add) ? (UQ_ADD | UQ_ENABLE) : UQ_DELETE;
	UQ_SET(&ue, pid, UQFILT_PROC, add, QNOTE_EXIT, 0, 0);

	return uevent(uq, &ue, 1, 0, 0, 0);
}

void test_proc(int uq)
{
	int i;
	int c;
	struct uevent ue;

	for (i = 0 ; 64 > i ; i++) {
		if (0 > (c = fork())) {
			xerror("Failed to fork");
		}
		if (0 == c) {
			exit(0);
		}

		if (0 > uqctl(uq, c, 1)) {
			xerror("Failed to register child");
		}

		printf("Monitoring %i\n", c);
	}

	while (0 != i) {
		if (1 != wpoll(uq, &ue, 1)) {
			xerror("Polling queue");
		}
		printf("%i died\n", ue.ident);
		i--;
	}
}

/* ========================================================================== */
