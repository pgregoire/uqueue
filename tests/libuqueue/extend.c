/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */

struct test {
	const char*	name;
	int		ndata;
	int		nff;
	int		ncntl;
	int		edata;
	int		eff;
	int		eres;
};


static void __test_cntl(int uq, short fl, const struct test* pt)
{
	int e;
	int da;
	int ff;
	struct uevent ue;

	ff = (QNOTE_TRIGGER | pt->nff | pt->ncntl);
	da = pt->ndata;

	UQ_SET(&ue, 0, fl, 0, ff, da, 0);
	if (1 != uevent(uq, &ue, 1, &ue, 1, &nilto)) {
		xerror("uevent returned nothing");
	}

	e = (0 != (UQ_ERROR & ue.flags));

	if ( ((0 != e) && (0 < pt->eres)) ||
	     ((0 == e) && (0 > pt->eres)) )
	{
		xerror("return value not matching");
	}
	if (0 != e) {
		return;
	}

	if (pt->edata != ue.data) {
		printf("e=%x,r=%x\n", pt->edata, ue.data);
		xerror("data not matching");
	}

	if (pt->eff != ue.fflags) {
		printf("e=%x,r=%x\n", pt->eff, ue.fflags);
		xerror("fflags not matching");
	}
}


#define FFLAGS		0xFFFF00
#define ORAND		(QNOTE_FFOR | QNOTE_FFAND)

static const struct test tests[] = {
	{ "data",	1,	0,			0,
			1, 	0,			1 },
	{ "ffand",	0,	0x2a2a2a,		QNOTE_FFAND,
			0,	(0x2a2a2a & FFLAGS),	1 },
	{ "ffor",	0,	0x2a2a2a,		QNOTE_FFOR,
			0,	(0x2a2a2a | FFLAGS),	1 },
	{ "ffcopy",	0,	0x2a2a2a,		QNOTE_FFCOPY,
			0,	0x2a2a2a,		1 },
	{ "mcntl",	0,	0,			ORAND,
			0,	0,			-1 },
	{ "ncntl",	0,	0,			0,
			0,	0,			1 }
};

static void test_cntl(int uq)
{
	int i;
	struct uevent ue;

	UQ_SET(&ue, 0, UQFILT_EXTENDMIN, (UQ_ADD|UQ_ENABLE), FFLAGS, 0, 0);
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("Couldn't register event");
	}

	for (i = 0 ; NARRAY(tests) > i ; i++) {
		printf("Testing '%s'\n", tests[i].name);
		__test_cntl(uq, UQFILT_EXTENDMIN, &tests[i]);
	}

	ue.flags = UQ_DELETE;
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("Couldn't deregister event");
	}
}

/* -------------------------------------------------------------------------- */
/* Identification range */

static void test_range(int uq)
{
	int i;
	struct uevent ue;

	for (i = UQFILT_EXTENDMIN ; UQFILT_EXTENDMAX >= i ; i++) {
		UQ_SET(&ue, 0, i, UQ_ADD, 0, 0, 0);
		if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
			xerror("Failed to register event");
		}
		ue.flags = UQ_DELETE;
		if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
			xerror("Failed to deregister event");
		}
	}
}

/* -------------------------------------------------------------------------- */

void test_extend(int uq)
{
	test_cntl(uq);
	test_range(uq);
}

/* ========================================================================== */
