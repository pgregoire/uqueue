/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/stat.h>

#include <stdlib.h>

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */

static void test_vnode_delete(int uq, int fd, const char* fp)
{
	struct uevent ue;

	UQ_SET(&ue, fd, UQFILT_VNODE, (UQ_ADD | UQ_ENABLE), QNOTE_DELETE, 0, 0);
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[delete] uevent/set");
	}

	if (0 > unlink(fp)) {
		xerror("[delete] unlink");
	}

	if (1 != qpoll(uq, &ue, 1)) {
		xerror("[delete] uevent/get");
	}
	if (QNOTE_DELETE != ue.fflags) {
		xerror("[delete] mismatch");
	}

	ue.flags = UQ_DELETE;
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[delete] uevent/del");
	}
}

static void test_vnode_rename(int uq, int fd, const char* op, char* np)
{
	struct uevent ue;

	if (0 == tmpnam(np)) {
		xerror("[rename] tmpnam");
	}

	UQ_SET(&ue, fd, UQFILT_VNODE, (UQ_ADD | UQ_ENABLE), QNOTE_RENAME, 0, 0);
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[rename] uevent/set");
	}

	if (0 > rename(op, np)) {
		xerror("[rename] rename");
	}

	if (1 != qpoll(uq, &ue, 1)) {
		xerror("[rename] uevent/get");
	}
	if (QNOTE_RENAME != ue.fflags) {
		xerror("[rename] mismatch");
	}

	ue.flags = UQ_DELETE;
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[rename] uevent/del");
	}
}

static void test_vnode_attrib(int uq, int fd, int md)
{
	struct uevent ue;

	UQ_SET(&ue, fd, UQFILT_VNODE, (UQ_ADD | UQ_ENABLE), QNOTE_ATTRIB, 0, 0);
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[attrib] uevent/set");
	}

	if (0 != fchmod(fd, md)) {
		xerror("[attrib] fchmod");
	}

	if (1 != qpoll(uq, &ue, 1)) {
		xerror("[attrib] uevent/get");
	}

	ue.flags = UQ_DELETE;
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[attrib] uevent/del");
	}
}

static void test_vnode_write_file(int uq, int fd)
{
	struct uevent ue;

	UQ_SET(&ue, fd, UQFILT_VNODE, (UQ_ADD | UQ_ENABLE), QNOTE_WRITE, 0, 0);
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[write/file] uevent/set");
	}

	if (sizeof(ue) != write(fd, &ue, sizeof(ue))) {
		xerror("[write] write");
	}

	if (1 != qpoll(uq, &ue, 1)) {
		xerror("[write/file] uevent/get");
	}
	if (QNOTE_WRITE != ue.fflags) {
		xerror("[write/file] mismatch");
	}

	ue.flags = UQ_DELETE;
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[write/file] uevent/del");
	}
}


static char p1[] = "/tmp/uq-vnode.XXXXXX";
static char p2[] = "/tmp/uq-vnode.XXXXXX";

static void vnode_atexit(void)
{
	(void)unlink(p1);
	(void)unlink(p2);
}

void test_vnode(int uq)
{
	int dd;
	int fd;
	struct uevent ue;

	if (0 > (dd = open("/tmp", O_RDONLY))) {
		xerror("open");
	}

	UQ_SET(&ue, dd, UQFILT_VNODE, (UQ_ADD | UQ_ENABLE), QNOTE_WRITE, 0, 0);
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[write] uevent/set");
	}

	if (0 > (fd = mkstemp(p1))) {
		xerror("[write] mkstemp");
	}

	if (1 != qpoll(uq, &ue, 1)) {
		xerror("[write] uevent/get");
	}
	if (QNOTE_WRITE != ue.fflags) {
		xerror("[write] mismatch");
	}

	ue.flags = UQ_DELETE;
	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("[write] uevent/del");
	}


	test_vnode_write_file(uq, fd);
	test_vnode_attrib(uq, fd, 0440);
	test_vnode_rename(uq, fd, p1, p2);
	test_vnode_write_file(uq, fd);
	test_vnode_attrib(uq, fd, 0400);
	test_vnode_delete(uq, fd, p2);

	(void)close(fd);
	(void)unlink(p2);
}

/* ========================================================================== */
