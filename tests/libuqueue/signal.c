/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <signal.h>
#include <string.h>

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */
/* Delivery count */

#define SIGTEST		SIGRTMIN

/* Returns non-zero if the signal was catched @n times. */
static int wascatched(int uq, int n)
{
	int r;
	struct uevent ue;

	if (0 > (r = qpoll(uq, &ue, 1))) {
		xerror("Failed to poll system");
	}

	if (0 == n) {
		return (0 == r);
	}

	return (n == ue.data);
}

static void test_signal_count(int uq)
{
	struct uevent ue;

	UQ_SET(&ue, SIGTEST, UQFILT_SIGNAL, UQ_ADD|UQ_ENABLE, 0, 0, 0);
	if (0 > uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("Failed to register source");
	}

	if (0 == wascatched(uq, 0)) {
		xerror("Premature detection");
	}

	raise(SIGTEST);
	raise(SIGTEST);

	if (0 == wascatched(uq, 2)) {
		xerror("Failed detection");
	}

	ue.flags = UQ_DELETE;
	if (0 > uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("Failed to deregister source");
	}
}

/* -------------------------------------------------------------------------- */
/* Registerable signals */

static const int sval[] = {
	SIGABRT, SIGALRM, SIGBUS, SIGCONT, SIGFPE, SIGHUP,
	SIGILL, SIGINT, SIGPIPE, SIGQUIT, SIGSEGV, SIGTERM,
	SIGTSTP, SIGTTIN, SIGTTOU, SIGUSR1, SIGUSR2,
	SIGPROF, SIGSYS, SIGTRAP, SIGURG, SIGVTALRM,
	SIGXCPU, SIGXFSZ
};

static const int sinval[] = {
	0, SIGKILL, SIGSTOP, SIGCHLD
};

static void __test_regis_sig2(int uq, int sn, int ex)
{
	struct uevent ue;

	printf("> Trying to register %s (expecting %s)\n",
	       strsignal(sn), (0 == ex) ? "success" : "error");

	UQ_SET(&ue, sn, UQFILT_SIGNAL, UQ_ADD|UQ_ENABLE, 0, 0, 0);
	if (ex != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("Failed to register signal");
	}
	if (0 == ex) {
		ue.flags = UQ_DELETE;
		if (0 > uevent(uq, &ue, 1, 0, 0, 0)) {
			xerror("Failed to deregister signal");
		}
	}
}

static void __test_regis_sig(int uq, int sn, int ex)
{
	if (UQ_SIGNO_AIO == sn) {
		return;
	}

	__test_regis_sig2(uq, sn, ex);
}


static void __test_regis_buf(int uq, const int* buf, size_t sz, int ex)
{
	size_t i;

	for (i = 0 ; sz > i ; i++) {
		__test_regis_sig(uq, buf[i], ex);
	}
}

static void test_signal_regis(int uq)
{
	int i;

	__test_regis_buf(uq, sval, NARRAY(sval), 0);
	__test_regis_buf(uq, sinval, NARRAY(sinval), -1);
	__test_regis_sig2(uq, UQ_SIGNO_AIO, -1);

	for (i = SIGRTMIN ; SIGRTMAX >= i ; i++) {
		__test_regis_sig(uq, i, 0);
	}
}

/* -------------------------------------------------------------------------- */

void test_signal(int uq)
{
	test_signal_count(uq);
	test_signal_regis(uq);
}

/* ========================================================================== */
