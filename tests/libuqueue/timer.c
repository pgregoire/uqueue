/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <signal.h>
#include <string.h>

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */

static struct {
	int	u;
	int	v;
} timers[NTIMERS];

static uint64_t ticks[NTIMERS][NTICKS];

/* -------------------------------------------------------------------------- */

static int newival(int u)
{
	return (random() % 100);
}

static int i2u(int idx)
{
	idx %= 3;

	switch (idx) {
	case 0: return QNOTE_MSECONDS;
	case 1: return QNOTE_USECONDS;
	default: break;
	}

	return QNOTE_NSECONDS;
}

static int armctl(int uq, int add, int id, int unit, int value)
{
	struct uevent ue;

	add = (0 != add) ? (UQ_ADD | UQ_ENABLE) : UQ_DELETE;
	UQ_SET(&ue, id, UQFILT_TIMER, add, unit, value, 0);

	return uevent(uq, &ue, 1, 0, 0, 0);
}

static int arm(int uq, int nt)
{
	int i;

	for (i = 0 ; nt > i ; i++) {
		timers[i].u = i2u(i);
		timers[i].v = newival(timers[i].u);
		timers[i].v = (0 == timers[i].v) ? 1 : timers[i].v;

		if (0 != armctl(uq, 1, i, timers[i].u, timers[i].v)) {
			return -1;
		}
	}

	return 0;
}

static int getntick(int i)
{
	int j;

	for (j = 0 ; NTICKS > j ; j++) {
		if (0 == ticks[i][j]) {
			return j;
		}
	}

	return -1;
}

static int poll(int uq, int nt)
{
	int i;
	int j;
	int n;
	int o;
	uint64_t t;
	struct uevent ue[128];

	(void)memset(ticks, 0, sizeof(ticks));

	for (i = 0 ; (NTICKS * nt) > i ; /**/) {
		if (0 >= (o = uevent(uq, 0, 0, ue, NARRAY(ue), 0))) {
			return -1;
		}

		t = now();
		for (j = 0 ; o > j ; j++) {
			if (0 <= (n = getntick(ue[j].ident))) {
				ticks[ue[j].ident][n++] = t;
				i++;
				if (NTICKS == n) {
					(void)armctl(uq, 0, ue[j].ident, 0, 0);
				}
			}
		}
	}

	return 0;
}

static unsigned long long dm(unsigned long long v, int m)
{
	int i;

	for (i = 0 ; m > i ; i++) {
		v /= 10;
	}

	return v;
}

static void __prstat(unsigned long long v)
{
	printf("%llu,%llu,%llu,%llu\n", v, dm(v, 3), dm(v, 6), dm(v, 9));
}

static uint64_t getival(int u, int v)
{
	switch (u) {
	case QNOTE_NSECONDS: return (uint64_t)v;
	case QNOTE_USECONDS: return (uint64_t)v * 1000;
	case QNOTE_MSECONDS: return (uint64_t)v * 1000000;
	case QNOTE_SECONDS:  return (uint64_t)v * 1000000000;
	}

	return 0;
}

static void prstat(int nt)
{
	int i;
	int j;
	uint64_t a;
	uint64_t d;
	uint64_t l;
	uint64_t v;

	for (i = 0 ; nt > i ; i++) {
		v = getival(timers[i].u, timers[i].v);
		for (j = 0, a = 0, l = 0 ; NTICKS > j ; j++) {
			if (0 != j) {
				d = (ticks[i][j] - l);
				a += d;
			}
			l = ticks[i][j];
		}
		printf("%03i [%010llu] ", i, v);
		__prstat(a / (NTICKS - 1));
	}
}

/* -------------------------------------------------------------------------- */

static void mixed(int uq, int nt)
{
	if (0 != arm(uq, nt)) {
		xerror("[mixed] arm");
	}

	if (0 != poll(uq, nt)) {
		xerror("[mixed] poll");
	}

	prstat(nt);
}

void test_timer(int uq)
{
	int i;
	int n;

	srandom(getpid());

	i = (NTIMERS / 10);

	for (n = i ; NTIMERS >= n ; n += i) {
		mixed(uq, n);
	}
}

/* ========================================================================== */
