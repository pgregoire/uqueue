/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

#ifndef UQUEUE_TEST_H
#define UQUEUE_TEST_H
/* ========================================================================== */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <uqueue.h>

/* -------------------------------------------------------------------------- */

#define xerror(s)			\
do {	printf("%s: ", __func__);	\
	perror(s);			\
	exit(1);			\
} while (0)

#define NARRAY(a)	(sizeof(a) / sizeof((a)[0]))

#define TIME(block)						\
do {	uint64_t ___a, ___b;					\
	___a = now();						\
	block;							\
	__b = now();						\
	printf("%s took %llu ns.\n", __func__, ___b - ___a);	\
} while (0)

/* -------------------------------------------------------------------------- */

static const struct timespec nilto;

static uint64_t now(void)
{
	struct timespec ts;

	(void)clock_gettime(CLOCK_MONOTONIC, &ts);

	return ((uint64_t)ts.tv_sec * 1000000000) + ts.tv_nsec;
}

static int setnblock(int fd)
{
	int o;

	if (0 > (o = fcntl(fd, F_GETFL))) {
		return -1;
	}

	return fcntl(fd, F_SETFL, (o | O_NONBLOCK));
}

static int emptyfd(int fd)
{
	int n;
	char b[4096];

	while (0 < (n = read(fd, b, sizeof(b)))) {
		/**/;
	}

	if ((0 > n) && (EAGAIN == errno)) {
		return 0;
	}

	return n;
}

static int qpoll(int uq, struct uevent* ue, size_t ne)
{
	return uevent(uq, 0, 0, ue, ne, &nilto);
}

static int wpoll(int uq, struct uevent* ue, size_t ne)
{
	return uevent(uq, 0, 0, ue, ne, 0);
}

/* ========================================================================== */
#endif /* ! UQUEUE_TEST_H */
