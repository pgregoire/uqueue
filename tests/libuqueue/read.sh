#!/bin/sh
## ========================================================================== ##
##
## This file is part of uqueue.
##
## Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
## All rights reserved.
##
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##
## 1. Redistributions of source code must retain the above copyright notice,
##    this list of conditions and the following disclaimer.
## 2. Redistributions in binary form must reproduce the above copyright notice,
##    this list of conditions and the following disclaimer in the documentation
##    and/or other materials provided with the distribution.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
## ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
## LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
## INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
## CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
## POSSIBILITY OF SUCH DAMAGE.
##
## The views and conclusions contained in the software and documentation are
## those of the authors and should not be interpreted as representing official
## policies, either expressed or implied, of the uqueue project.
##
## ========================================================================== ##
D=$(cd $(dirname "$0"); pwd)

if [ 1 -ne $# ]; then
	printf 'usage: %s <uq-tests path>\n' $(basename "$0")
	exit 1
fi


PROG="$1"

_tmp=tmp.$$
cleanup() {
	rm -f "$_tmp"*
}
trap cleanup EXIT INT QUIT TERM

## -------------------------------------------------------------------------- ##

STR='Hello, World\n'

# xcmp <a> <b>
xcmp() {
#	if ! cmp "$1" "$2"; then
#		xxd "$1"
#		xxd "$2"
#		exit 1
#	fi
	cmp "$1" "$2" || exit 1
}

n=0
logtest() {
	n=$(expr $n \+ 1)
	printf '==> Running test: READ %i (%s)\n' $n "$1" >&2
}


logtest 'redirection'
"$PROG" /dev/stdin < "$0" 2> "$_tmp"
xcmp "$0" "$_tmp"

logtest 'pipe'
printf "$STR" > "$_tmp.1"
printf "$STR" | "$PROG" /dev/stdin 2> "$_tmp.2"
xcmp "$_tmp.1" "$_tmp.2"

logtest 'regular file'
for f in hosts passwd; do
	if [ -f "/etc/$f" ]; then
		"$PROG" "/etc/$f" 2> "$_tmp"
		xcmp "/etc/$f" "$_tmp"
		break
	fi
done

## ========================================================================== ##
exit 0
