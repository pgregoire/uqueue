/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/types.h>
#include <sys/stat.h>

#include <string.h>

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */

static int pr(int fd)
{
	int n;
	char b[4096];

	if (0 > (n = read(fd, b, sizeof(b)))) {
		xerror("read");
	} else if (0 == n) {
		return -1;
	}

	b[n] = '\0';
	fprintf(stderr, "%s", b);

	return 0;
}

static int armctl(int uq, int fd, int add)
{
	struct uevent ue;

	add = (0 != add) ? (UQ_ADD | UQ_ENABLE) : UQ_DELETE;
	UQ_SET(&ue, fd, UQFILT_READ, add, 0, 0, 0);

	return uevent(uq, &ue, 1, 0, 0, 0);
}


void test_read(int uq, int argc, const char* const* argv)
{
	int n;
	int fd;
	struct uevent ue;

	if (0 > (fd = open(argv[1], (O_RDONLY | O_NONBLOCK)))) {
		xerror("Failed to open file");
	}

	if (0 > armctl(uq, fd, 1)) {
		xerror("Failed to register file");
	}

	do {
		if (0 > (n = wpoll(uq, &ue, 1))) {
			xerror("Failed to poll system");
		} else if (0 < n) {
			if (0 != pr(fd)) {
				break;
			}
		}
	} while (1);

	if (0 > armctl(uq, fd, 0)) {
		xerror("Failed to deregister file");
	}
	(void)close(fd);
}

/* ========================================================================== */
