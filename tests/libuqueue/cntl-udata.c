/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <unistd.h>

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */

static int uqctl(int uq, int fd, int op, int ud)
{
	struct uevent ue;

	if (0 != (UQ_ADD & op)) {
		op |= UQ_ENABLE;
	}

	UQ_SET(&ue, fd, UQFILT_READ, op, 0, 0, ud);
	return uevent(uq, &ue, 1, 0, 0, 0);
}

static int test(int uq, long xd)
{
	struct uevent ue;

	if (1 != qpoll(uq, &ue, 1)) {
		xerror("qpoll");
	}

	return -(xd != (long)ue.udata);
}

void test_cntl_udata(int uq)
{
	int i;
	int pd[2];

	if (0 > pipe(pd)) {
		xerror("pipe");
	}
	if (sizeof(pd) != write(pd[1], pd, sizeof(pd))) {
		xerror("write");
	}
	if (0 > uqctl(uq, pd[0], UQ_ADD, 0)) {
		xerror("uevent/add");
	}

	for (i = 0 ; 4 > i ; /**/) {
		if (0 > test(uq, i)) {
			xerror("udata mismatch");
		}
		if (0 > uqctl(uq, pd[0], 0, ++i)) {
			xerror("uqctl/mod");
		}
	}

	if (0 > uqctl(uq, pd[0], UQ_DELETE, 0)) {
		xerror("uqctl/delete");
	}

	(void)close(pd[0]);
	(void)close(pd[1]);
}

/* ========================================================================== */
