/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <fcntl.h>
#include <unistd.h>

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */

static int uqctl(int uq, int fd, int add)
{
	struct uevent ue;

	add = (0 != add) ? (UQ_ADD | UQ_ENABLE) : UQ_DELETE;

	UQ_SET(&ue, fd, UQFILT_READ, add, 0, 0, 0);

	return uevent(uq, &ue, 1, 0, 0, 0);
}

static int __setup(int uq, int* bf)
{
	int t;

	if (0 > pipe(bf)) {
		return -1;
	}
	if (0 > setnblock(bf[0])) {
		return -1;
	}
	if (0 > uqctl(uq, bf[0], 1)) {
		return -1;
	}
	if (sizeof(t) != write(bf[1], &t, sizeof(t))) {
		return -1;
	}

	return 0;
}

static int setup(int uq, int* p1, int* p2, int add)
{
	if (0 != add) {
		if (0 > __setup(uq, p1)) {
			return -1;
		}
		return __setup(uq, p2);
	}

	if (0 > uqctl(uq, p1[0], 0)) {
		return -1;
	}
	return uqctl(uq, p2[0], 0);
}

static int empty(int* p1, int* p2)
{
	if (0 > emptyfd(p1[0])) {
		return -1;
	}
	return emptyfd(p2[0]);
}

void test_cntl_qstate(int uq)
{
	int p1[2];
	int p2[2];
	struct uevent ue;

	if (0 > setup(uq, p1, p2, 1)) {
		xerror("setup/add");
	}

	if (1 != qpoll(uq, &ue, 1)) {
		xerror("uevent");
	}

	if (0 > empty(p1, p2)) {
		xerror("empty");
	}

	if (0 != qpoll(uq, &ue, 1)) {
		xerror("uevent not nil");
	}

	if (0 > setup(uq, p1, p2, 0)) {
		xerror("setup/del");
	}
}

/* ========================================================================== */
