/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <string.h>

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */

static int fillup(int fd)
{
	int n;
	char b[4096];

	while (0 < (n = write(fd, b, sizeof(b)))) {
		/**/;
	}

	if ((0 > n) && (EAGAIN == errno)) {
		return 0;
	}

	return n;
}

static int iswriteable(int uq, int fd)
{
	struct uevent ue;

	if (1 != qpoll(uq, &ue, 1)) {
		return 0;
	}

	return (fd == ue.ident);
}

static int __setup(int uq, int fd, int add)
{
	struct uevent ue;

	add = (0 != add) ? (UQ_ADD | UQ_ENABLE) : UQ_DELETE;
	UQ_SET(&ue, fd, UQFILT_WRITE, add, 0, 0, 0);

	if (0 != UQ_DELETE) {
		if (0 != setnblock(fd)) {
			xerror("Failed to set non-blocking");
		}
		return uevent(uq, &ue, 1, 0, 0, 0);
	}

	if (0 != uevent(uq, &ue, 1, 0, 0, 0)) {
		xerror("Failed to deregister filter");
	}
	return close(fd);
}

static void setup(int uq, int* fds, int add)
{
	if (0 != add) {
		if (0 > pipe(fds)) {
			xerror("Failed to create pipe");
		}
	}

	if (0 > __setup(uq, fds[0], add)) {
		xerror("Failed to setup 0");
	}
	if (0 > __setup(uq, fds[1], add)) {
		xerror("Failed to setup 1");
	}
}


void test_write(int uq)
{
	int fds[2];

	setup(uq, fds, 1);

	if (0 == iswriteable(uq, fds[1])) {
		xerror("Failed early writeable check");
	}
	if (0 != fillup(fds[1])) {
		xerror("Failed to fill pipe");
	}

	if (0 != iswriteable(uq, fds[1])) {
		xerror("Filled pipe is writeable");
	}

	if (0 != emptyfd(fds[0])) {
		xerror("Failed to empty pipe");
	}
	if (0 == iswriteable(uq, fds[1])) {
		xerror("Emptied pipe is not writeable");
	}

	setup(uq, fds, 0);
}

/* ========================================================================== */
