/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <aio.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "uqueue-test.h"

/* -------------------------------------------------------------------------- */

static char fname[] = "/tmp/uqaio-test.XXXXXX";
static const char dat[] = "Hello, World!";
static volatile char buf[4096];

/* -------------------------------------------------------------------------- */

static void test_aio_read(int uq, int fd)
{
	struct aiocb cb;
	struct uevent ue;

	(void)memset(&cb, 0, sizeof(cb));
	cb.aio_fildes = fd;
	cb.aio_buf    = buf;
	cb.aio_nbytes = sizeof(buf);

	if (0 > uq_aio_read(uq, &cb)) {
		xerror("Submitting request");
	}

	if (1 != wpoll(uq, &ue, 1)) {
		xerror("Polling uqueue");
	}

	if ((UQFILT_AIO != ue.filter) || ((intptr_t)&cb != ue.ident)) {
		xerror("Checking notification");
	}

	printf("Read: %s\n", cb.aio_buf);
}

static void test_aio_write(int uq, int fd)
{
	struct aiocb cb;
	struct uevent ue;

	(void)memset(&cb, 0, sizeof(cb));
	cb.aio_fildes = fd;
	cb.aio_buf    = (void*)dat;
	cb.aio_nbytes = sizeof(dat);

	if (0 > uq_aio_write(uq, &cb)) {
		xerror("Submitting request");
	}

	if (1 != wpoll(uq, &ue, 1)) {
		xerror("Polling queue");
	}

	if ( (UQFILT_AIO != ue.filter) || ((intptr_t)&cb != ue.ident) ||
	     (sizeof(dat) != ue.data) )
	{
		xerror("Checking notification");
	}

	printf("Wrote: %s\n", cb.aio_buf);
}

static void test_aio_fsync(int uq, int fd, int op)
{
	struct aiocb cb;
	struct uevent ue;

	(void)memset(&cb, 0, sizeof(cb));
	cb.aio_fildes = fd;

	if (0 != uq_aio_fsync(uq, op, &cb)) {
		xerror("Submitting request");
	}

	if (1 != wpoll(uq, &ue, 1)) {
		xerror("Polling queue");
	}
}

/* -------------------------------------------------------------------------- */

static void __atexit(void)
{
	(void)unlink(fname);
}

void test_aio(int uq)
{
	int fd;

	if (0 > (fd = mkstemp(fname))) {
		xerror("mkstemp");
	}

	(void)atexit(__atexit);

	test_aio_write(uq, fd);
	test_aio_read(uq, fd);
	test_aio_fsync(uq, fd, O_SYNC);
	test_aio_fsync(uq, fd, O_DSYNC);
}

/* ========================================================================== */
