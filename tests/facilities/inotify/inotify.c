/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/inotify.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define xerror(msg)		do { perror(msg); exit(1); } while (0)

/* -------------------------------------------------------------------------- */

static const char* strevent(int mask)
{
#define CM(v)						\
do { if (0 != ((v) & mask)) {				\
		strncat(b, # v "," , sizeof(b));	\
	}						\
} while (0)

	static char b[8192];

	b[0] = '\0';
	CM(IN_ACCESS);
	CM(IN_ATTRIB);
	CM(IN_CLOSE_WRITE);
	CM(IN_CLOSE_NOWRITE);
	CM(IN_CREATE);
	CM(IN_DELETE);
	CM(IN_DELETE_SELF);
	CM(IN_MODIFY);
	CM(IN_MOVE_SELF);
	CM(IN_MOVED_FROM);
	CM(IN_MOVED_TO);
	CM(IN_OPEN);

	return b;
}

static void dumpev(const struct inotify_event* ie)
{
	if (0 != ie) {
		printf("wd:     %i\n", ie->wd);
		printf("mask:   %08x (%s)\n", ie->mask, strevent(ie->mask));
		printf("cookie: %08x\n", ie->cookie);
		printf("len:    %u\n", ie->len);
		if (0 != ie->len) {
			printf("name:   '%s'\n", ie->name);
		}
	}
}

static void dumpevs(int ifd)
{
	int i;
	int n;
	char b[4096];
	struct inotify_event* p;

	while (0 < (n = read(ifd, b, sizeof(b)))) {
		i = 0;
		while (i < n) {
			p  = (struct inotify_event*)&b[i];
			dumpev(p);
			i += (sizeof(*p) + p->len);
		}
	}
}

/* -------------------------------------------------------------------------- */

int main(int argc, char** const argv)
{
	int c;
	int i;
	int id;
	int op;

	op = 0;
	while (-1 != (c = getopt(argc, argv, "o"))) {
		switch (c) {
		case 'o': op = 1; break;
		default: break;
		}
	}

	if (optind == argc) {
		fprintf(stderr, "usage: [-o] %s <path>...\n", argv[0]);
		return 1;
	}

	fprintf(stderr, "^C to terminate\n");

	if (0 > (id = inotify_init1(0))) {
		xerror("inotify_init1");
	}

	for (i = optind ; argc > i ; i++) {
		fprintf(stderr, "==> Adding '%s'\n", argv[i]);
		if (0 != op) {
			if (0 > open(argv[i], O_RDONLY)) {
				xerror("open");
			}
		}
		if (0 > inotify_add_watch(id, argv[i], IN_ALL_EVENTS)) {
			xerror("inotify_add_watch");
		}
	}

	dumpevs(id);

	/* XXX NEVER REACHED */
	return 0;
}

/* ========================================================================== */
