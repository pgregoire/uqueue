#!/bin/sh
##
D=$(cd $(dirname "$0"); pwd)

if [ ! -x "$D/signalfd" ]; then
	printf 'Cannot locate program signalfd\n' >&2
	exit 1
fi

_tmp=tmp.$$
cu() {
	rm -f "$_tmp"
}
trap cu EXIT QUIT INT TERM


$D/signalfd >$_tmp &
p=$!

for s in FPE ILL SEGV BUS TRAP SYS; do
	kill -$s $p || :
done

kill -9 $p || :
cat $_tmp 2>/dev/null || :

exit 0
