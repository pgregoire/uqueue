/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/signalfd.h>

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#define xerror(msg)	do { perror(msg); exit(1); } while (0)

#define OPTS		"hkmqr"
#define M_KILL		1
#define M_QUEUE		2
#define M_RAISE		3

/* -------------------------------------------------------------------------- */

static void usage(const char* const pn, int xc)
{
	fprintf(stderr, "usage: %s [-%s]\n", pn, OPTS);
	exit(xc);
}

static void _raise(int mt)
{
	int i;
	int p;
	sigset_t ss;
	union sigval v;

	p = getpid();
	(void)sigfillset(&ss);

	for (i = 1 ; 128 > i ; i++) {
		if (0 == sigismember(&ss, i)) {
			continue;
		}
		if ((SIGINT == i) || (SIGKILL == i) || (SIGSTOP == i)) {
			continue;
		}
		printf("Delivering (%i) %s\n", i, strsignal(i));
		switch (mt) {
		case M_KILL:  (void)kill(p, i); break;
		case M_RAISE: (void)raise(i); break;
		case M_QUEUE: (void)sigqueue(p, i, v); break;
		}
	}
}

int main(int argc, char* const* argv)
{
	int fd;
	int om;
	int rm;
	sigset_t ss;
	struct signalfd_siginfo si;

	om = rm = 0;

	while (-1 != (fd = getopt(argc, argv, OPTS))) {
		switch (fd) {
		case 'h': usage(argv[0], 0); break;
		case 'k': rm = M_KILL; break;
		case 'm': om = 1; break;
		case 'q': rm = M_QUEUE; break;
		case 'r': rm = M_RAISE; break;
		default:  usage(argv[0], 1); break;
		}
	}
	if (optind != argc) {
		usage(argv[0], 1);
	}

	setbuf(stdout, 0);

	printf("# pid: %i\n", getpid());
	printf("# SIGINT (^C, %i) will be left unmasked\n", SIGINT);


	(void)sigfillset(&ss);
	(void)sigdelset(&ss, SIGINT);
	if (0 > (fd = signalfd(-1, &ss, 0))) {
		xerror("signalfd");
	}

	if (0 != om) {
		if (0 > sigprocmask(SIG_SETMASK, &ss, 0)) {
			xerror("sigprocmask");
		}
		puts("# signals were masked");
	}

	if (0 != rm) {
		puts("# delivering signals");
		_raise(rm);
		puts("# delivered signals");
	}

	do {
		if (0 > read(fd, &si, sizeof(si))) {
			xerror("read");
		}
		om = si.ssi_signo;
		printf("caught: (%i) %s\n", om, strsignal(om));
	} while (1);

	return 0;
}

/* ========================================================================== */
