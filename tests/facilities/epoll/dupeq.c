/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/epoll.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define xerror(s)	do { perror(s); exit(1); } while (0)

/* -------------------------------------------------------------------------- */

int main(int argc, const char* const* argv)
{
	int d;
	int n;
	int e;
	int p[2];
	char b[128];
	struct epoll_event ee;

	if (1 == argc) {
		fprintf(stderr, "usage: %s <msg>\n", argv[0]);
		return 1;
	}

	if (0 != pipe(p)) {
		xerror("pipe");
	}
	printf("created pipe [0]=%i, [1]=%i\n", p[0], p[1]);

	if (0 > (d = dup(p[0]))) {
		xerror("dup");
	}
	printf("dup-ed read-end of the pipe=%i\n", d);

	if (0 > (e = epoll_create(128))) {
		xerror("epoll_create");
	}

	ee.events  = EPOLLIN;
	ee.data.fd = p[0];
	if (0 != epoll_ctl(e, EPOLL_CTL_ADD, ee.data.fd, &ee)) {
		xerror("epoll_ctl");
	}
	puts("preparing to monitor read-end of pipe");

	if (0 >= write(p[1], argv[1], strlen(argv[1]))) {
		xerror("write");
	}

	(void)close(p[0]);
	(void)close(p[1]);
	puts("closed pipe");

	if (0 > (p[0] = dup(d))) {
		xerror("[2] dup");
	}
	printf("dup-ed duplicate=%i\n", p[0]);
	(void)close(d);
	puts("closed first duplicate");

	puts("polling");
	if (1 == (n = epoll_wait(e, &ee, 1, -1))) {
		printf("got event on %i\n", ee.data.fd);
		if (0 <= (n = read(ee.data.fd, b, sizeof(b)))) {
			b[n] = '\0';
			printf(">> read: '%s'\n", b);
		} else {
			xerror("read");
		}
	} else if (0 == n) {
		puts("no events");
	} else {
		xerror("epoll_wait");
	}

	return 0;
}

/* ========================================================================== */
