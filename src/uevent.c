/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <errno.h>
#include <string.h>

#include "control.h"

/* -------------------------------------------------------------------------- */

static int __uchange_delete(struct unote* un)
{
	if (0 == un) {
		errno = ENOENT;
		return -1;
	}

	(void)fdetach(un);
	qdequeue(un);
	qremove(un);

	return 0;
}

static int
__uchange(struct uqueue* uq, struct unote* un, const struct uevent* ue)
{
	int e;

	if (0 != (UQ_DELETE & ue->flags)) {
		return __uchange_delete(un);
	}

	if (0 != fchange(ue)) {
		return -1;
	}

	if (0 != (UQ_ADD & ue->flags)) {
		if (0 != un) {
			errno = EEXIST;
			return -1;
		}
		if (0 == (un = qinsert(uq, ue))) {
			return -1;
		}
	} else if (0 == un) {
		errno = ENOENT;
		return -1;
	}

	if (0 == fattach(un, ue)) {
		un->ue.flags = (ue->flags | (un->ue.flags & UQ_SYSFLAGS));
		un->ue.udata = ue->udata;
		return 0;
	}

	e = errno;
	if (0 != (UQ_ADD & ue->flags)) {
		qremove(un);
	}

	errno = e;
	return -1;
}

int uchange(struct uqueue* uq, const struct uevent* ue)
{
#define __ADDDEL	(UQ_ADD | UQ_DELETE)
#define __ENADIS	(UQ_ENABLE | UQ_DISABLE)
#define __DISONE	(UQ_DISABLE | UQ_ONESHOT)

	struct unote* n;

	if ( (__ADDDEL == (__ADDDEL & ue->flags)) ||
	     (__ENADIS == (__ENADIS & ue->flags)) ||
	     (__DISONE == (__DISONE & ue->flags)) )
	{
		errno = EINVAL;
		return -1;
	}

	if (0 == uq) {
		uq = getsysq();
	}

	if (0 != (n = ufind(ue->filter, ue->ident))) {
		if (uq != n->uq) {
			errno = EEXIST;
			return -1;
		}
	}

	return __uchange(uq, n, ue);
}

/* -------------------------------------------------------------------------- */

static int __unotify(struct unote* un, const struct uevent* ue)
{
	int r;

	if (0 > (r = fnotify(un, ue))) {
		return -1;
	}

	if (0 != r) {
		qenqueue(un);
	}

	return 0;
}

int unotify(const struct uevent* ue)
{
	struct uevent e;
	struct unote* n;

	if (0 == (n = ufind(ue->filter, ue->ident))) {
		return -1;
	}

	(void)memcpy(&e, ue, sizeof(e));
	if (0 != (UQ_CHAIN & n->ue.flags)) {
		e.filter = (short)(long)n->ue.udata;
	}

	return __unotify(n, &e);
}

/* -------------------------------------------------------------------------- */

static int readq(struct uqueue* uq, struct uevent* buf, size_t cnt)
{
	size_t i;
	struct unote* p;

	for (i = 0 ; cnt > i ; i++) {
		if (0 == (p = qpop(uq))) {
			break;
		}

		(void)memcpy(&buf[i], &p->ue, sizeof(*buf));
		(void)fcopyout(&buf[i], p);

		buf[i].flags &= ~UQ_STATE;

		if (0 != (UQ_DISPATCH & p->ue.flags)) {
			p->ue.flags |= UQ_DISABLE;
		} else if (0 != (UQ_ONESHOT & p->ue.flags)) {
			p->ue.flags = UQ_DELETE;
			(void)uchange(p->uq, &p->ue);
		}
		/* XXX UQ_EOF: delete? */
	}

	qclean(uq);

	return (int)i;
}

static int __fpoll(uint64_t rlim)
{
	int e;
	int i;
	int n;
	struct uevent b[UQUEUE_MAXPOLL];

	if (0 != qtimedlock(getsysq(), rlim)) {
		return 0;
	}

	if (0 > (n = fpoll(b, NARRAY(b), rlim))) {
		if (ENOENT != errno) {
			goto L_UNLOCK;
		}
		n = nsleep(0, rlim);
	}

L_UNLOCK:
	e = errno;
	qunlock(getsysq());
	errno = e;

	if (0 > n) {
		return -1;
	}

	for (i = 0 ; n > i ; i++) {
		// XXX ufind(); fdetach()
		(void)unotify(&b[i]);
	}

	return 0;
}

static int
__upoll(struct uqueue* uq, struct uevent* buf, size_t cnt, uint64_t alim)
{
	int n;
	uint64_t t;

	t = 0;

	do {
		if (0 > (n = __fpoll(t))) {
			if ((EINTR != errno) && (ETIMEDOUT != errno)) {
				break;
			}
		}

		if (0 != (n = readq(uq, buf, cnt))) {
			break;
		}
		t = (-1 == alim) ? -1 : (alim - now());
		if (t > alim) {
			break;
		}
	} while (1);

	return n;
}

static int upoll(struct uqueue* uq, struct uevent* buf, size_t cnt,
                 const struct timespec* to)
{
	uint64_t t;

	if (0 == cnt) {
		if ((0 == to) || (0 == ts2u64(to))) {
			return 0;
		}
		return nsleep(to, 0);
	}

	t = (0 != to) ? (now() + ts2u64(to)) : -1;

	return __upoll(uq, buf, cnt, t);
}

/* -------------------------------------------------------------------------- */

static int __change(struct uqueue* uq, const struct uevent* ue)
{
	if (0 != (UQ_SYSFLAGS & ue->flags)) {
		errno = EINVAL;
	}
	return uchange(uq, ue);
}

static int change(struct uqueue* uq, const struct uevent* changes,
                  size_t nchanges, struct uevent* events, size_t nevents)
{
	int e;
	size_t i;
	size_t j;

	for (i = 0, j = 0 ; nchanges > i ; i++) {
		if (0 == __change(uq, &changes[i])) {
			continue;
		}

		if (nevents <= j) {
			return -1;
		}

		e = errno;
		(void)memcpy(&events[j], &changes[i], sizeof(*events));
		events[j].data   = e;
		events[j].flags |= UQ_ERROR;
		j++;
	}

	return (int)j;
}

DSO_EXPORT(int uevent(int, const struct uevent*, size_t,
                      struct uevent*, size_t, const struct timespec*));

int uevent(int queue, const struct uevent* changes, size_t nchanges,
           struct uevent* events, size_t nevents,
           const struct timespec* timeout)
{
	int n;
	struct uqueue* q;

	if ( ((0 != nchanges) && (0 == changes)) ||
	     ((0 != nevents)  && (0 == events)) )
	{
		errno = EINVAL;
		return -1;
	}

	if (0 == (q = getqueue(queue))) {
		return -1;
	}

	if (0 != (n = change(q, changes, nchanges, events, nevents))) {
		return n;
	}

	return upoll(q, events, nevents, timeout);
}

/* ========================================================================== */
