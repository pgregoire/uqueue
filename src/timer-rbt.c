/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <errno.h>
#include <semaphore.h>

#include "uqueue-tree.h"

#include "filters.h"

#if UQUEUE_HERTZ >= 1000000000
# error "Does not support clock resolution >= 1 BHz"
#endif /* UQUEUE_HERTZ >= 1000000000 */

/* -------------------------------------------------------------------------- */

struct timer {
	int		armed;
	uintptr_t	id;
	uint64_t	xp;

	RB_ENTRY(timer)	rbe;
};

static sem_t			seml;
static sem_t			semt;
static struct timer		timers[UQUEUE_MAXTIMERS];
static RB_HEAD(trb, timer)	rbt = RB_INITIALIZER(&rbt);

/* -------------------------------------------------------------------------- */

static int cmptimer(const struct timer* a, const struct timer* b)
{
	/* Allow insertion of similar values.  */
	return (a->xp == b->xp) ? -1 : (a->xp - b->xp);
}

RB_GENERATE_STATIC(trb, timer, rbe, cmptimer)

/* -------------------------------------------------------------------------- */

#define L_LOCK()		\
do {	int ___e = errno;	\
	(void)sem_wait(&seml);	\
	errno = ___e;		\
} while (0)

#define L_UNLOCK()		\
do {	int ___e = errno;	\
	(void)sem_post(&seml);	\
	errno = ___e;		\
} while (0)

#define T_LOCK()		\
do {	int ___e = errno;	\
	(void)sem_wait(&semt);	\
	errno = ___e;		\
} while (0)

#define T_UNLOCK()		\
do {	int ___e = errno;	\
	(void)sem_post(&semt);	\
	errno = ___e;		\
} while (0)

#define rb_insert(t)				\
do {	if (0 == (t)->armed) {			\
		RB_INSERT(trb, &rbt, (t));	\
		(t)->armed = 1;			\
	}					\
} while (0)

#define rb_remove(t)				\
do {	if (0 != (t)->armed) {			\
		RB_REMOVE(trb, &rbt, (t));	\
		(t)->armed = 0;			\
	}					\
} while (0)

/* -------------------------------------------------------------------------- */

static int findfree(uintptr_t id)
{
	int i;

	for (i = 0 ; NARRAY(timers) > i ; i++) {
		if (0 == timers[i].xp) {
			timers[i].id = id;
			return i;
		}
	} 

	errno = ENOMEM;
	return -1;
}

static int findent(uintptr_t id, unsigned int i)
{
	if (NARRAY(timers) > i) {
		if ((id == timers[i].id) && (0 != timers[i].xp)) {
			return i;
		}
	}

	errno = ENOENT;
	return -1;
}

/*
 * The following equation computes the interval between
 * the last timer expiration (pt->xp) and the next; or,
 * in other words, the distance between two (2) points.
 */
static uint64_t getd2n(uint64_t last, uint64_t _now, uint64_t ival)
{
	if (_now >= last) {
		return ((((_now - last) / ival) + 1) * ival);
	}
	return last;
}

static int arm(struct timer* pt, uint64_t epoch, uint64_t ival)
{
	if (0 != pt) {
		T_LOCK();

		rb_remove(pt);
		pt->xp += getd2n(pt->xp, epoch, ival);
		rb_insert(pt);

		T_UNLOCK();
		return 0;
	}

	errno = EINVAL;
	return -1;
}

/* -------------------------------------------------------------------------- */

static int attach(struct unote* un, const struct uevent* ue, uint64_t iv)
{
	int t;

	if (0 > (t = findent(ue->ident, un->nfflags))) {
		if (0 > (t = findfree(ue->ident))) {
			return -1;
		}
		un->nfflags = t;
	}

	if (0 != (UQ_ADD & ue->flags)) {
		return 1;
	}

	return ((0 != iv) && (uf_timer_getival(&un->ue) != iv));
}

/*
 * nil ival	:= acceptable if timer exists, leave as is
 * new ival	:= rearm with new ival, must be dequeued XXX
 */
int uf_timer_rbt_attach(struct unote* un, const struct uevent* ue)
{
	int m;
	uint64_t i;

	m = 1;
	i = uf_timer_getival(ue);

	L_LOCK();
	if (0 < (m = attach(un, ue, i))) {
		timers[un->nfflags].xp = now();
	}
	L_UNLOCK();

	if (0 >= m) {
		return m;
	}

	arm(&timers[un->nfflags], timers[un->nfflags].xp, i);

	return 0;
}

int uf_timer_rbt_detach(const struct unote* un)
{
	T_LOCK();
	L_LOCK();

	if (un->nfflags == findent(un->ue.ident, un->nfflags)) {
		rb_remove(&timers[un->nfflags]);
		timers[un->nfflags].xp = 0;
	}

	L_UNLOCK();
	T_UNLOCK();

	return 0;
}

int uf_timer_rbt_copyout(struct uevent* ue, struct unote* un)
{
	uint64_t n;
	uint64_t v;

	n = now();
	v = uf_timer_getival(&un->ue);

	ue->data = (n - timers[un->nfflags].xp) / v;
	arm(&timers[un->nfflags], n, v);

	return 0;
}

/* -------------------------------------------------------------------------- */

static int xpop(uint64_t ct, struct uevent* ue, int rm)
{
	struct timer* t;

	if (0 != (t = RB_MIN(trb, &rbt))) {
		if (ct >= t->xp) {
			if (0 != ue) {
				ue->filter = UQFILT_TIMER;
				ue->ident  = t->id;
			}
			if (0 != rm) {
				rb_remove(t);
			}
			return 0;
		}
	}

	errno = EAGAIN;
	return -1;
}

int uf_timer_rbt_poll(struct uevent* ue, size_t sz, uint64_t to)
{
	int i;
	uint64_t t;

	T_LOCK();
	t = now();

	if ((0 == ue) && (0 == sz) && (0 == to)) {
		i = xpop(t, 0, 0);
	} else {
		for (i = 0 ; sz > i ; i++) {
			if (0 != xpop(t, &ue[i], 1)) {
				break;
			}
		}
	}
	T_UNLOCK();

	return i;
}

/* -------------------------------------------------------------------------- */

void uf_timer_rbt_exit(void)
{
	(void)sem_destroy(&seml);
	(void)sem_destroy(&semt);
}

int uf_timer_rbt_init(void)
{
	int e;

	if (0 > sem_init(&seml, 1, 1)) {
		return -1;
	}
	if (0 == sem_init(&semt, 1, 1)) {
		return 0;
	}

	e = errno;
	(void)sem_destroy(&seml);
	errno = e;

	return -1;
}

/* ========================================================================== */
