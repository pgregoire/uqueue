/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "control.h"

/* -------------------------------------------------------------------------- */
/* allocation */

static int init_lock(struct uqueue* uq)
{
	int e;
	pthread_mutexattr_t a;

	if (0 != (e = pthread_mutexattr_init(&a))) {
		errno = e;
		return -1;
	}

	if (0 != (e = pthread_mutexattr_settype(&a, PTHREAD_MUTEX_RECURSIVE))) {
		(void)pthread_mutexattr_destroy(&a);
		errno = e;
		return -1;
	}
	if (0 != (e = pthread_mutex_init(&uq->lock, &a))) {
		(void)pthread_mutexattr_destroy(&a);
		errno = e;
		return -1;
	}

	return 0;
}

struct uqueue* qcreate(void)
{
	struct uqueue* p;

	if (0 == (p = calloc(1, sizeof(*p)))) {
		return 0;
	}

	if (0 == init_lock(p)) {
		SLIST_INIT(&p->lhead);
		TAILQ_INIT(&p->qhead);
		return p;
	}

	free(p);

	return 0;
}

void qdestroy(struct uqueue* uq)
{
	struct unote* p;

	while (0 != (p = SLIST_FIRST(&uq->lhead))) {
		SLIST_REMOVE_HEAD(&uq->lhead, sle);
		(void)fdetach(p);
		free(p);
	}

	(void)pthread_mutex_destroy(&uq->lock);
	free(uq);
}

/* -------------------------------------------------------------------------- */
/* locking */

void qlock(struct uqueue* uq)
{
	(void)pthread_mutex_lock(&uq->lock);
}

int qtimedlock(struct uqueue* uq, uint64_t rt)
{
	struct timespec ts;

	rt += now();

	ts.tv_sec  = (rt / 1000000000);
	ts.tv_nsec = (rt % 1000000000);

	return -(0 != (errno = pthread_mutex_timedlock(&uq->lock, &ts)));
}

void qunlock(struct uqueue* uq)
{
	(void)pthread_mutex_unlock(&uq->lock);
}

/* -------------------------------------------------------------------------- */
/* registrations */

struct unote* qinsert(struct uqueue* uq, const struct uevent* ue)
{
	struct unote* p;

	if (0 == (p = calloc(1, sizeof(*p)))) {
		return 0;
	}

	(void)memcpy(&p->ue, ue, sizeof(p->ue));
	p->uq = uq;

	qlock(uq);
	SLIST_INSERT_HEAD(&uq->lhead, p, sle);
	qunlock(uq);

	return p;
}

void qremove(struct unote* un)
{
	qlock(un->uq);
	SLIST_REMOVE(&un->uq->lhead, un, unote, sle);
	qunlock(un->uq);

	free(un);
}

/* -------------------------------------------------------------------------- */
/* notifications */

void qenqueue(struct unote* un)
{
	int i;
	struct unote* p;

	i = 1;

	qlock(un->uq);
	TAILQ_FOREACH(p, &un->uq->qhead, tqe) {
		if (un == p) {
			i = 0;
			break;
		}
	}
	if (0 != i) {
		TAILQ_INSERT_TAIL(&un->uq->qhead, un, tqe);
	}
	qunlock(un->uq);
}

void qdequeue(struct unote* un)
{
	struct unote* p;

	qlock(un->uq);
	TAILQ_FOREACH(p, &un->uq->qhead, tqe) {
		if (un == p) {
			TAILQ_REMOVE(&un->uq->qhead, p, tqe);
			break;
		}
	}
	qunlock(un->uq);
}

struct unote* qpop(struct uqueue* uq)
{
	struct unote* p;

	qlock(uq);
	TAILQ_FOREACH(p, &uq->qhead, tqe) {
		if (0 != (UQ_DISABLE & p->ue.flags)) {
			continue;
		}

		TAILQ_REMOVE(&uq->qhead, p, tqe);
		break;
	}
	qunlock(uq);

	if (0 == p) {
		errno = EAGAIN;
	}

	return p;
}

void qclean(struct uqueue* uq)
{
	struct unote* p;
	struct unote* t;

	qlock(uq);
	TAILQ_FOREACH_SAFE(p, &uq->qhead, tqe, t) {
		if (0 != (UQ_STATE & p->ue.flags)) {
			TAILQ_REMOVE(&uq->qhead, p, tqe);
		}
	}
	qunlock(uq);
}

/* -------------------------------------------------------------------------- */

static int eval(const struct uscan* us, struct unote* un)
{
#define FIND		(SF_FILTER | SF_IDENT)

	if (FIND == (FIND & us->sflags)) {
		if ( (us->filter != un->ue.filter) ||
		     (us->ident  != un->ue.ident)
		) {
			return 0;
		}
	} else if (0 != (SF_FILTER & us->sflags)) {
		if (us->filter != un->ue.filter) {
			return 0;
		}
	}

	if (0 != us->eval) {
		return us->eval(un, us->earg);
	}

	return 1;
}

struct unote* qscan(struct uqueue* uq, const struct uscan* us)
{
	int e;
	struct unote* p;

	e = 0;
	p = 0;

	if (0 != uq) {
		qlock(uq);
		SLIST_FOREACH(p, &uq->lhead, sle) {
			if (0 != (e = eval(us, p))) {
				break;
			}
		}
		qunlock(uq);
	}

	if (0 > e) {
		return (struct unote*)-1;
	}

	return p;
}

/* ========================================================================== */
