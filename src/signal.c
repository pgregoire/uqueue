/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/wait.h>

#include <errno.h>
#include <signal.h>
#include <string.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

int uf_signal_sigchld(siginfo_t* si)
{
	int p;
	int s;

	if (0 != si) {
		if (0 < (p = waitpid(-1, &s, WNOHANG))) {
			(void)memset(si, 0, sizeof(*si));
			si->si_pid    = p;
			si->si_signo  = SIGCHLD;
			if (0 != WIFEXITED(s)) {
				si->si_code   = WEXITED;
				si->si_status = WEXITSTATUS(s);
			}

			return 0;
		}
	}

	return -1;
}

/* -------------------------------------------------------------------------- */

static sigset_t cset;

int uf_signal_iscatchable(int sn)
{
	return (0 < sigismember(&cset, sn));
}

/* -------------------------------------------------------------------------- */

static int hasfdata(const struct uevent* ue)
{
	return ((0 != ue->data) || (0 != ue->fflags));
}
static int isresvd(int sn)
{
	/* XXX We should also check for UQ_SIGNO_AIO */
	return (SIGCHLD == sn);
}
static int hasresvd(const struct uevent* ue)
{
	return ((0 == (UQ_CHAIN & ue->flags)) && (0 != isresvd(ue->ident)));
}

static int uf_signal_change(const struct uevent* ue)
{
	if ( (0 != hasfdata(ue)) || (0 != hasresvd(ue)) ||
	     (0 == uf_signal_iscatchable(ue->ident)) )
	{
		errno = EINVAL;
		return -1;
	}

	return 0;
}

/* -------------------------------------------------------------------------- */

static const struct ufilter* cfilt;

static int uf_signal_attach(struct unote* un, const struct uevent* ue)
{
	return uf_attach(cfilt, un, ue);
}

static int uf_signal_detach(const struct unote* un)
{
	return uf_detach(cfilt, un);
}

static int uf_signal_notify(struct unote* un, const struct uevent* ue)
{
	return uf_notify(cfilt, un, ue);
}

static int uf_signal_copyout(struct uevent* ue, struct unote* un)
{
	return uf_copyout(cfilt, ue, un);
}

/* -------------------------------------------------------------------------- */

static void uf_signal_exit(void)
{
	if (0 != cfilt) {
		uf_exit(cfilt);
		cfilt = 0;
	}
}

static void setcatchable(void)
{
	/*
	 * Signals that can be caught, from a libc perspective.
	 * Signals reserved by the libc are automatically removed.
	 */
	(void)sigfillset(&cset);
	(void)sigdelset(&cset, 0);
	(void)sigdelset(&cset, SIGKILL);
	(void)sigdelset(&cset, SIGSTOP);
}


extern const struct ufilter uf_signal_sigaction;
extern const struct ufilter uf_signal_signalfd;

static int uf_signal_init(void)
{
	if (0 == cfilt) {
		if (0 == uf_init(&uf_signal_signalfd)) {
			cfilt = &uf_signal_signalfd;
		} else if (0 == uf_init(&uf_signal_sigaction)) {
			cfilt = &uf_signal_sigaction;
		} else {
			return -1;
		}

		setcatchable();
	}

	return 0;
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_signal = {
	.init    = uf_signal_init,
	.exit    = uf_signal_exit,
	.change  = uf_signal_change,
	.attach  = uf_signal_attach,
	.detach  = uf_signal_detach,
	.notify  = uf_signal_notify,
	.copyout = uf_signal_copyout
};

/* ========================================================================== */
