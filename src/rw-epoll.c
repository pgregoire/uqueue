/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/*
 * WARNING
 *
 * Because epoll is a mess, the only way to prevent it from
 * returning incorrect events is to remove descriptors when
 * they are closed. This measure prevents duplicates from
 * generating events. This agrees with uqueue's usage
 * guidelines.
 *
 * Also, since there is no safe way to modify epoll's watch
 * following uqueue's notion of read and write events, all
 * descriptors are watched for I/O, irrespective of the real
 * application interests. We rely on unotify() to distinguish
 * between what events are of interest.
 */

/* ========================================================================== */

#include "filters.h"

#ifdef USE_EPOLL
#include <sys/epoll.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "uqueue-tree.h"

/* -------------------------------------------------------------------------- */
/*
 * XXX It would be more efficient to have a dedicated
 *     red-black tree implementation.
 */

struct ent {
	int	fd;
	int	events;

	RB_ENTRY(ent)	rbe;
};


static int			epfd = -1;
static RB_HEAD(erb, ent)	rbt = RB_INITIALIZER(&rbt);

/* -------------------------------------------------------------------------- */

static int cmpent(const struct ent* a, const struct ent* b)
{
	return (a->fd - b->fd);
}

RB_GENERATE_STATIC(erb, ent, rbe, cmpent);

/* -------------------------------------------------------------------------- */

static void cpy(struct uevent* ue, int fd, uint16_t fl)
{
	ue->ident  = fd;
	ue->filter = fl;
}

static int __stor(struct uevent* buf, size_t cnt)
{
	size_t i;
	struct ent* p;

	i = 0;

	RB_FOREACH(p, erb, &rbt) {
		if (cnt <= i) {
			break;
		}
		if (0 != (EPOLLIN & p->events)) {
			cpy(&buf[i++], p->fd, UQFILT_READ);
		}
		if (cnt <= i) {
			break;
		}
		if (0 != (EPOLLOUT & p->events)) {
			cpy(&buf[i++], p->fd, UQFILT_WRITE);
		}
	}

	return i;
}

static int
stor(struct uevent* buf, size_t cnt, const struct epoll_event* ee, int nelems)
{
	int e;
	size_t c;

	for (e = 0, c = 0 ; (cnt > c) && (nelems > e) ; e++) {
		if (0 != ((EPOLLIN | EPOLLHUP) & ee[e].events)) {
			cpy(&buf[c++], ee[e].data.fd, UQFILT_READ);
		}
		if (cnt <= c) {
			break;
		}
		if (0 != (EPOLLOUT & ee[e].events)) {
			cpy(&buf[c++], ee[e].data.fd, UQFILT_WRITE);
		}
	}

	return (c + __stor(&buf[c], (cnt - c)));
}


static int uf_rw_epoll_poll(struct uevent* buf, size_t cnt, uint64_t to)
{
	int n;
	struct epoll_event ee[128];	// XXX Increase at compile-time

	if (0 != RB_MIN(erb, &rbt)) {
		to = 0;
	}

	if (0 > (n = epoll_wait(epfd, ee, NARRAY(ee), (to / 1000000)))) {
		return 0;
	}

	return stor(buf, cnt, ee, n);
}

/* -------------------------------------------------------------------------- */

static int atde(int fd, int add)
{
	struct epoll_event ee;

	add = (0 != add) ? EPOLL_CTL_ADD : EPOLL_CTL_DEL;

	ee.data.fd = fd;
	ee.events  = (EPOLLIN | EPOLLOUT);

	return epoll_ctl(epfd, add, fd, &ee);
}

static int ep_insert(const struct unote* un)
{
	int f;
	struct ent* p;

	f = (UQFILT_READ == un->ue.filter) ? EPOLLIN : EPOLLOUT;

	if (0 == (p = (struct ent*)un->ndata)) {
		if (0 == (p = calloc(1, sizeof(*p)))) {
			return -1;
		}
		p->fd = un->ue.ident;
		RB_INSERT(erb, &rbt, p);
	}

	p->events |= f;

	return 0;
}

static int uf_rw_epoll_attach(struct unote* un, const struct uevent* ue)
{
	if (0 == (UQ_ADD & ue->flags)) {
		return 0;
	}

	if (0 > atde(ue->ident, 1)) {
		if (EPERM != errno) {
			return -1;
		}

		if (0 > ep_insert(un)) {
			return -1;
		}

		un->ue.flags &= ~UQ_STATE;
		return unotify(&un->ue);
	}

	return 0;
}


static int ep_remove(const struct unote* un)
{
	int f;
	struct ent* p;

	f = (UQFILT_READ == un->ue.filter) ? EPOLLIN : EPOLLOUT;

	if (0 != (p = (struct ent*)un->ndata)) {
		if (f == p->events) {
			RB_REMOVE(erb, &rbt, p);
			free(p);
		} else {
			p->events &= ~f;
		}
	}

	return 0;
}

static int uf_rw_epoll_detach(const struct unote* un)
{
	if (0 == (UQ_STATE & un->ue.flags)) {
		return ep_remove(un);
	}
	return atde(un->ue.ident, 0);
}

/* -------------------------------------------------------------------------- */

static int uf_rw_epoll_init(void)
{
	if (0 > epfd) {
		return (epfd = epoll_create(1));
	}

	return 0;
}

static void uf_rw_epoll_exit(void)
{
	if (0 <= epfd) {
		(void)close(epfd);
		epfd = -1;
	}
}

/* -------------------------------------------------------------------------- */
#endif /* USE_EPOLL */

const struct ufilter uf_rw_epoll =
#ifdef USE_EPOLL
{
	.init    = uf_rw_epoll_init,
	.exit    = uf_rw_epoll_exit,
	.attach  = uf_rw_epoll_attach,
	.detach  = uf_rw_epoll_detach,
	.poll    = uf_rw_epoll_poll
};
#else /* ! USE_EPOLL */
UF_DUMMY_INITIALIZER;
#endif /* USE_EPOLL */

/* ========================================================================== */
