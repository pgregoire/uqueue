/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

#ifndef UQUEUE_FILTERS_H
#define UQUEUE_FILTERS_H
/* ========================================================================== */

#include "uqueue-priv.h"

/* -------------------------------------------------------------------------- */

struct ufilter {
	int	(*init)(void);
	void	(*exit)(void);
	int	(*change)(const struct uevent*);
	int	(*attach)(struct unote*, const struct uevent*);
	int	(*detach)(const struct unote*);
	int	(*poll)(struct uevent*, size_t, uint64_t);
	int	(*notify)(struct unote*, const struct uevent*);
	int	(*copyout)(struct uevent*, struct unote*);
};


extern int uf_dummy_init(void);

#define UF_DUMMY_INITIALIZER		{ .init = uf_dummy_init }

/* -------------------------------------------------------------------------- */

extern int  uf_init(const struct ufilter*);
extern void uf_exit(const struct ufilter*);

extern int  uf_attach(const struct ufilter*,
                      struct unote*, const struct uevent*);

extern int  uf_detach(const struct ufilter*, const struct unote* un);

extern int  uf_poll(const struct ufilter*, struct uevent*, size_t, uint64_t to);

extern int  uf_notify(const struct ufilter*,
                      struct unote*, const struct uevent*);

extern int  uf_copyout(const struct ufilter*, struct uevent*, struct unote*);

/* -------------------------------------------------------------------------- */

extern int uf_signal_sigchld(siginfo_t*);
extern int uf_signal_iscatchable(int);

/* timer */
extern int  uf_timer_rbt_init(void);
extern void uf_timer_rbt_exit(void);
extern int  uf_timer_rbt_attach(struct unote*, const struct uevent*);
extern int  uf_timer_rbt_detach(const struct unote* un);
extern int  uf_timer_rbt_poll(struct uevent*, size_t, uint64_t);
extern int  uf_timer_rbt_copyout(struct uevent*, struct unote*);

extern uint64_t uf_timer_getival(const struct uevent*);

/* ========================================================================== */
#endif /* ! UQUEUE_FILTERS_H */
