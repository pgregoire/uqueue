/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <errno.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

#define FFCNTLMASK	(QNOTE_TRIGGER | QNOTE_FFOR | QNOTE_FFAND | \
			 QNOTE_FFANDOR | QNOTE_FFCOPY)

static int uf_extend_change(const struct uevent* ue)
{
	int s;

	if (0 == (~FFCNTLMASK & (QNOTE_FFCNTLMASK & ue->fflags))) {
		s = 0;
		if (0 != (QNOTE_TRIGGER & ue->fflags)) {
			s += (0 != (QNOTE_FFAND   & ue->fflags)) ? 1 : 0;
			s += (0 != (QNOTE_FFANDOR & ue->fflags)) ? 1 : 0;
			s += (0 != (QNOTE_FFOR    & ue->fflags)) ? 1 : 0;
			s += (0 != (QNOTE_FFCOPY  & ue->fflags)) ? 1 : 0;

			if (1 < s) {
				goto X_EINVAL;
			}
		}

		return 0;
	}

X_EINVAL:
	errno = EINVAL;
	return -1;
}

static int uf_extend_attach(struct unote* un, const struct uevent* ue)
{
	if (0 != (QNOTE_TRIGGER & ue->fflags)) {
		return unotify(ue);
	}

	return 0;
}

static int uf_extend_notify(struct unote* un, const struct uevent* ue)
{
	unsigned int c;
	unsigned int f;
	unsigned int n;

	c = (ue->fflags & QNOTE_FFCNTLMASK) & ~QNOTE_TRIGGER;
	f = (ue->fflags & QNOTE_FFLAGSMASK);
	n = un->nfflags;

	switch (c) {
	case QNOTE_FFAND:
		n = (un->ue.fflags & f);
		break;
	case QNOTE_FFANDOR:
		n |= (un->ue.fflags & f);
		break;
	case QNOTE_FFCOPY:
		n = f;
		break;
	case QNOTE_FFOR:
		n = (un->ue.fflags | f);
		break;
	default:
		break;
	}

	un->ndata   = ue->data;
	un->nfflags = n;

	return 1;
}

static int uf_extend_copyout(struct uevent* ue, struct unote* un)
{
	ue->data   = un->ndata;
	ue->fflags = un->nfflags;

	un->ndata   = 0;
	un->nfflags = 0;

	return 0;
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_extend = {
	.change  = uf_extend_change,
	.attach  = uf_extend_attach,
	.notify  = uf_extend_notify,
	.copyout = uf_extend_copyout
};

/* ========================================================================== */
