/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <errno.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

extern const struct ufilter uf_rw;
extern const struct ufilter uf_signal;
extern const struct ufilter uf_timer;
extern const struct ufilter uf_proc;
extern const struct ufilter uf_aio;
extern const struct ufilter uf_vnode;
extern const struct ufilter uf_extend;

static const struct ufilter* const filts[] = {
	&uf_rw,			/* READ */
	0,			/* WRITE */
	&uf_signal,		/* SIGNAL */
	&uf_timer,		/* TIMER */
	&uf_proc,		/* PROC */
	&uf_aio,		/* AIO */
	&uf_vnode		/* VNODE */
};

/* -------------------------------------------------------------------------- */

static int islibfilt(short fl)
{
	return (NARRAY(filts) > fl);
}

static int isusrfilt(short fl)
{
	return ((UQFILT_EXTENDMIN <= fl) && (UQFILT_EXTENDMAX >= fl));
}


static const struct ufilter* fgetfilt(short fl)
{
	if (0 != islibfilt(fl)) {
		if (UQFILT_WRITE == fl) {
			fl = UQFILT_READ;
		}
		return filts[fl];
	} else if (0 != isusrfilt(fl)) {
		return &uf_extend;
	}

	errno = EINVAL;
	return 0;
}

static const struct ufilter* getfilt(const struct uevent* ue)
{
	if (0 == ue) {
		errno = EINVAL;
		return 0;
	}

	return fgetfilt(ue->filter);
}

/* -------------------------------------------------------------------------- */

int fattach(struct unote* un, const struct uevent* ue)
{
	const struct ufilter* f;

	if (0 == (f = getfilt(ue))) {
		return -1;
	}

	return (0 != f->attach) ? f->attach(un, ue) : 0;
}

int fchange(const struct uevent* ue)
{
	const struct ufilter* f;

	if (0 == (f = getfilt(ue))) {
		return -1;
	}

	return (0 != f->change) ? f->change(ue) : 0;
}

int fdetach(const struct unote* un)
{
	const struct ufilter* f;

	if (0 == (f = getfilt(&un->ue))) {
		return -1;
	}

	return (0 != f->detach) ? f->detach(un) : 0;
}

int fpoll(struct uevent* ue, size_t sz, uint64_t to)
{
	int i;
	int j;
	int n;

	i = (NARRAY(filts) - 1);
	j = 0;
	n = 0;

	for (/**/ ; 1 < i ; i--) {
		if (0 != filts[i]->poll) {
			if (0 < (n = filts[i]->poll(&ue[j], (sz - j), 0))) {
				j += n;
			}
		}

		if (j == (int)sz) {
			return j;
		}
	}

	if (0 != j) {
		to = 0;
	}

	if (0 < (n = filts[0]->poll(&ue[j], (sz - j), to))) {
		j += n;
	}

	return j;
}

int fnotify(struct unote* un, const struct uevent* ue)
{
	const struct ufilter* f;

	if (0 == (f = getfilt(ue))) {
		return -1;
	}

	return (0 != f->notify) ? f->notify(un, ue) : 0;
}

int fcopyout(struct uevent* ue, struct unote* un)
{
	const struct ufilter* f;

	if (0 == (f = getfilt(&un->ue))) {
		return -1;
	}

	return (0 != f->copyout) ? f->copyout(ue, un) : 0;
}

/* -------------------------------------------------------------------------- */

int uf_init(const struct ufilter* uf)
{
	if ((0 != uf) && (0 != uf->init)) {
		return uf->init();
	}
	return 0;
}

void uf_exit(const struct ufilter* uf)
{
	if ((0 != uf) && (0 != uf->exit)) {
		uf->exit();
	}
}

int
uf_attach(const struct ufilter* uf, struct unote* un, const struct uevent* ue)
{
	if ((0 != uf) && (0 != uf->attach)) {
		return uf->attach(un, ue);
	}
	return 0;
}

int uf_detach(const struct ufilter* uf, const struct unote* un)
{
	if ((0 != uf) && (0 != uf->detach)) {
		return uf->detach(un);
	}
	return 0;
}

int uf_poll(const struct ufilter* uf, struct uevent* ue, size_t sz, uint64_t to)
{
	if ((0 != uf) && (0 != uf->poll)) {
		return uf->poll(ue, sz, to);
	}
	return 0;
}

int
uf_notify(const struct ufilter* uf, struct unote* un, const struct uevent* ue)
{
	/*
 	 * XXX It is still uncertain whether all
	 *     monitoring modules should provide
	 *     this routine or whether it should
	 *     be provided by the filter module.
	 */
	if ((0 != uf) && (0 != uf->notify)) {
		return uf->notify(un, ue);
	}
	return 1;
}

int uf_copyout(const struct ufilter* uf, struct uevent* ue, struct unote* un)
{
	if ((0 != uf) && (0 != uf->copyout)) {
		return uf->copyout(ue, un);
	}
	return 0;
}

/* -------------------------------------------------------------------------- */

int uf_dummy_init(void)
{
	return -1;
}


int init_filters(void)
{
	short f;

	for (f = 0 ; NARRAY(filts) > f ; f++) {
		if (UQFILT_WRITE != f) {
			if (0 != uf_init(filts[f])) {
				return -1;
			}
		}
	}

	return 0;
}

void exit_filters(void)
{
	int i;

	for (i = (NARRAY(filts) - 1) ; i <= 0 ; i--) {
		uf_exit(filts[i]);
	}
}

/* ========================================================================== */
