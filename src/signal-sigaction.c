/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/*
 * This monitoring module uses the pipe technique to relay
 * signal deliveries to the READ filter; allowing it to unblock
 * when a signal is received. The handler writes the siginfo
 * structure on the pipe.
 */

/* ========================================================================== */

#include <errno.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

static int pfds[2];
static sigset_t	sset;

/* -------------------------------------------------------------------------- */

/*
 * Standard signals are not queued and there is the
 * possibility that SIGCHLD gets delivered when we
 * are handling another instance of it.
 *
 * This routine reduces this probability of
 * missing deliveries.
 */
static void sigchld(int fd)
{
	siginfo_t i;

	while (0 == uf_signal_sigchld(&i)) {
		(void)write(fd, &i, sizeof(i));
	}
}

static void sighand(int sn, siginfo_t* si, void* _)
{
	int e;

	e = errno;

	(void)write(pfds[1], si, sizeof(*si));

	if (SIGCHLD == sn) {
		sigchld(pfds[1]);
	}

	errno = e;
}

static int atde(int add, int signo)
{
	struct sigaction sa;

	(void)memset(&sa, 0, sizeof(sa));
	if (0 != add) {
		sa.sa_sigaction = sighand;
		sa.sa_flags     = SA_SIGINFO;
	} else {
		sa.sa_handler   = SIG_DFL;
	}

	if (0 > sigaction(signo, &sa, 0)) {
		return -1;
	}

	if (0 != add) {
		(void)sigaddset(&sset, signo);
	} else {
		(void)sigdelset(&sset, signo);
	}

	return 0;
}

static int uf_signal_sigaction_attach(struct unote* un, const struct uevent* ue)
{
	return atde(1, ue->ident);
}

static int uf_signal_sigaction_detach(const struct unote* un)
{
	return atde(0, un->ue.ident);
}

/* -------------------------------------------------------------------------- */

/*
 * While it might appear that sending an event message
 * containing the address of the siginfo on the stack is
 * unsafe, unotify() is assumed not to return until the
 * message can safely be discarded; either because it was
 * handled by a monitoring module or rejected.
 */
static int readpipe(int fd)
{
	int i;
	int c;
	struct uevent n;
	siginfo_t b[128];

	if (0 >= (c = read(fd, b, sizeof(b)))) {
		if (EAGAIN != errno) {
			return -1;
		}
	}

	c /= sizeof(b[0]);
	n.filter = UQFILT_SIGNAL;

	for (i = 0 ; c > i ; i++) {
		n.ident = b[i].si_signo;
		n.data  = (uintptr_t)&b[i];
		(void)unotify(&n);
	}

	return 0;
}

static int
uf_signal_sigaction_notify(struct unote* un, const struct uevent* ue)
{
	if ((UQFILT_READ == un->ue.filter) && (pfds[0] == un->ue.ident)) {
		(void)readpipe(pfds[0]);
		return 0;
	}

	un->ndata++;

	return 1;
}

static int
uf_signal_sigaction_copyout(struct uevent* ue, struct unote* un)
{
	ue->data  = un->ndata;
	un->ndata = 0;

	return 0;
}

/* -------------------------------------------------------------------------- */

static int reset(void)
{
	int i;
	int r;

	for (i = 1 ; 0 < i ; i++) {
		if (0 > (r = sigismember(&sset, i))) {
			break;
		} else if (0 < r) {
			if (0 != atde(0, i)) {
				return -1;
			}
		}
	}

	return 0;
}

static int uf_signal_sigaction_init(void)
{
	if (0 > reset()) {
		return -1;
	}

	return setup_pipe(UQFILT_SIGNAL, pfds, 1);
}

static void uf_signal_sigaction_exit(void)
{
	(void)setup_pipe(UQFILT_SIGNAL, pfds, 0);
	(void)reset();
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_signal_sigaction = {
	.init    = uf_signal_sigaction_init,
	.exit    = uf_signal_sigaction_exit,
	.attach  = uf_signal_sigaction_attach,
	.detach  = uf_signal_sigaction_detach,
	.notify  = uf_signal_sigaction_notify,
	.copyout = uf_signal_sigaction_copyout
};

/* ========================================================================== */
