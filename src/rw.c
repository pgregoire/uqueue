/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/stat.h>

#include <errno.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

static int uf_rw_change(const struct uevent* ue)
{
	struct stat st;

	if ((0 != ue->data) || (0 != ue->fflags)) {
		errno = EINVAL;
		return -1;
	}

	if (0 != (UQ_ADD & ue->flags)) {
		if (0 > fstat(ue->ident, &st)) {
			if (EBADF == errno) {
				return -1;
			}
		}
	}

	return 0;
}


static const struct ufilter* cfilt;

static int uf_rw_attach(struct unote* un, const struct uevent* ue)
{
	un->ue.flags |= UQ_STATE;
	return uf_attach(cfilt, un, ue);
}

static int uf_rw_detach(const struct unote* un)
{
	return uf_detach(cfilt, un);
}

static int uf_rw_poll(struct uevent* ue, size_t sz, uint64_t to)
{
	return uf_poll(cfilt, ue, sz, to);
}

static int uf_rw_notify(struct unote* un, const struct uevent* ue)
{
	return uf_notify(cfilt, un, ue);
}

/* -------------------------------------------------------------------------- */

static void uf_rw_exit(void)
{
	if (0 != cfilt) {
		uf_exit(cfilt);
		cfilt = 0;
	}
}


extern const struct ufilter uf_rw_select;
extern const struct ufilter uf_rw_epoll;

static int uf_rw_init(void)
{
	if (0 == cfilt) {
		if (0 <= uf_init(&uf_rw_epoll)) {
			cfilt = &uf_rw_epoll;
		} else if (0 == uf_init(&uf_rw_select)) {
			cfilt = &uf_rw_select;
		} else {
			return -1;
		}
	}

	return 0;
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_rw = {
	.init   = uf_rw_init,
	.exit   = uf_rw_exit,
	.change = uf_rw_change,
	.attach = uf_rw_attach,
	.detach = uf_rw_detach,
	.poll   = uf_rw_poll,
	.notify = uf_rw_notify
};

/* ========================================================================== */
