/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

static int pfds[2] = { -1, -1 };

/* -------------------------------------------------------------------------- */

static int uf_timer_thread_notify(struct unote* un, const struct uevent* ue)
{
	int t;

	if ((UQFILT_READ == un->ue.filter) && (pfds[0] == un->ue.ident)) {
		while (0 < read(pfds[0], &t, sizeof(t))) {
			/**/;
		}
		return 0;
	}

	return 1;
}

/* -------------------------------------------------------------------------- */

static void prepare_loop(struct timespec* ts)
{
	sigset_t ss;

	(void)sigfillset(&ss);
	(void)pthread_sigmask(SIG_BLOCK, &ss, 0);

	ts->tv_sec  = 0;
	ts->tv_nsec = (1000000000 / UQUEUE_HERTZ);
}

static void* uf_timer_thread_loop(void* _)
{
	int t;
	struct timespec ts;

	prepare_loop(&ts);

	do {
		(void)clock_nanosleep(CLOCK_MONOTONIC, 0, &ts, 0);

		if (0 == uf_timer_rbt_poll(0, 0, 0)) {
			(void)write(pfds[1], &t, sizeof(t));
		}
	} while (1);

	return 0;
}

/* -------------------------------------------------------------------------- */

static pthread_t pth;

static void uf_timer_thread_exit(void)
{
	(void)pthread_cancel(pth);
	(void)setup_pipe(UQFILT_TIMER, pfds, 0);
	uf_timer_rbt_exit();
}

static int uf_timer_thread_init(void)
{
	int e;

	if (0 <= pfds[0]) {
		return 0;
	}

	if (0 != uf_timer_rbt_init()) {
		return -1;
	}
	if (0 > setup_pipe(UQFILT_TIMER, pfds, 1)) {
		goto X_ERR;
	}
	if (0 == (errno = pthread_create(&pth, 0, uf_timer_thread_loop, 0))) {
		return 0;
	}

X_ERR:
	e = errno;
	uf_timer_thread_exit();
	errno = e;

	return -1;
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_timer_thread = {
	.init    = uf_timer_thread_init,
	.exit    = uf_timer_thread_exit,
	.attach  = uf_timer_rbt_attach,
	.detach  = uf_timer_rbt_detach,
	.poll    = uf_timer_rbt_poll,
	.notify  = uf_timer_thread_notify,
	.copyout = uf_timer_rbt_copyout
};

/* ========================================================================== */
