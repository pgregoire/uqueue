/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <aio.h>
#include <errno.h>
#include <fcntl.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

#define OP_READ		0
#define OP_WRITE	1
#define OP_SYNC		2
#define OP_DSYNC	3

/* -------------------------------------------------------------------------- */

static int doop(int op, struct aiocb* cb)
{
	switch (op) {
	case OP_READ:
		return aio_read(cb);
	case OP_WRITE:
		return aio_write(cb);
	case OP_SYNC:
		return aio_fsync(O_SYNC, cb);
	case OP_DSYNC:
		return aio_fsync(O_DSYNC, cb);
	default:
		break;
	}

	/* XXX Should never occur */
	errno = EINVAL;
	return -1;

}

static int wrap(int uq, int op, struct aiocb* cb)
{
	int e;
	void* p;
	struct uevent u;

	if (0 == cb) {
		errno = EINVAL;
		return -1;
	}

	p = cb->aio_sigevent.sigev_value.sival_ptr;

	cb->aio_sigevent.sigev_notify          = SIGEV_SIGNAL;
	cb->aio_sigevent.sigev_signo           = UQ_SIGNO_AIO;
	cb->aio_sigevent.sigev_value.sival_ptr = cb;

	UQ_SET(&u, cb, UQFILT_AIO, (UQ_ADD | UQ_ENABLE), 0, 0, p);
	if (0 != uevent(uq, &u, 1, 0, 0, 0)) {
		return -1;
	}

	if (0 == doop(op, cb)) {
		return 0;
	}

	e = errno;
	u.flags = UQ_DELETE;
	(void)uevent(uq, &u, 1, 0, 0, 0);

	errno = e;
	return -1;
}

DSO_EXPORT(int uq_aio_read(int, struct aiocb*));
int uq_aio_read(int uq, struct aiocb* cb)
{
	return wrap(uq, OP_READ, cb);
}

DSO_EXPORT(int uq_aio_write(int, struct aiocb*));
int uq_aio_write(int uq, struct aiocb* cb)
{
	return wrap(uq, OP_WRITE, cb);
}

DSO_EXPORT(int uq_aio_fsync(int, int, struct aiocb*));
int uq_aio_fsync(int uq, int op, struct aiocb* cb)
{
	if (O_SYNC == op) {
		return wrap(uq, OP_SYNC, cb);
	} else if (O_DSYNC == op) {
		return wrap(uq, OP_DSYNC, cb);
	}

	errno = EINVAL;
	return -1;
}

/* -------------------------------------------------------------------------- */

DSO_EXPORT(int uq_aio_error(const struct aiocb*));
int uq_aio_error(const struct aiocb* cb)
{
	int e;
	struct unote* n;

	if (0 == (n = ufind(UQFILT_AIO, (uintptr_t)cb))) {
		errno = EINVAL;
		return -1;
	}

	switch (e = aio_error(cb)) {
	case EINPROGRESS:
	case ECANCELED:		/* FIXME Deletion worth? */
	case 0:
		return e;
	default:
		break;
	}

	if (0 > e) {
		return -1;
	}

	n->ue.flags = UQ_DELETE;
	(void)uchange(n->uq, &n->ue);

	return e;
}

DSO_EXPORT(int uq_aio_return(struct aiocb*));
int uq_aio_return(struct aiocb* cb)
{
	int e;
	struct unote* n;

	if (0 == (n = ufind(UQFILT_AIO, (uintptr_t)cb))) {
		errno = EINVAL;
		return -1;
	}

	if (0 != (QNOTE_TRIGGER & n->nfflags)) {
		e = n->ndata;
	} else {
		if (0 > (e = aio_return(cb))) {
			return -1;
		}
	}

	n->ue.flags = UQ_DELETE;
	(void)uchange(n->uq, &n->ue);

	return e;
}

/* -------------------------------------------------------------------------- */

static int uf_aio_change(const struct uevent* ue)
{
	if ((0 != ue->data) || (0 != ue->fflags)) {
		errno = EINVAL;
		return -1;
	}
	return 0;
}

static const struct ufilter* cfilt;

static int uf_aio_notify(struct unote* un, const struct uevent* ue)
{
	return uf_notify(cfilt, un, ue);
}

static int uf_aio_copyout(struct uevent* ue, struct unote* un)
{
	if (0 == uf_copyout(cfilt, ue, un)) {
		un->ue.flags |= UQ_ONESHOT;
		return 0;
	}
	return -1;
}

/* -------------------------------------------------------------------------- */

static void uf_aio_exit(void)
{
	if (0 != cfilt) {
		uf_exit(cfilt);
		cfilt = 0;
	}
}


extern const struct ufilter uf_aio_signal;

static int uf_aio_init(void)
{
	if (0 == cfilt) {
		if (0 == uf_init(&uf_aio_signal)) {
			cfilt = &uf_aio_signal;
		} else {
			return -1;
		}
	}

	return 0;
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_aio = {
	.init    = uf_aio_init,
	.exit    = uf_aio_exit,
	.change  = uf_aio_change,
	.notify  = uf_aio_notify,
	.copyout = uf_aio_copyout
};

/* ========================================================================== */
