/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

#ifndef UQUEUE_H
#define UQUEUE_H
/* ========================================================================== */

#include <aio.h>
#include <signal.h>
#include <stdint.h>
#include <time.h>

/* -------------------------------------------------------------------------- */

/* flags */
#define UQ_ADD			0x0001
#define UQ_DELETE		0x0002
#define UQ_ENABLE		0x0004	/* Report events */
#define UQ_DISABLE		0x0008	/* Do not report events */
#define UQ_ONESHOT		0x0010	/* Delete after retrieval */
#define UQ_DISPATCH		0x0020	/* Disable after retrieval */
#define UQ_CLEAR		0x0040	/* Reset state after retrieval */
#define UQ_SYSFLAGS		0xF000
#define UQ_ERROR		0x8000
#define UQ_EOF			0x4000
#define UQ_CHAIN		0x2000
#define UQ_STATE		0x1000

/* filters */
#define UQFILT_READ		0
#define UQFILT_WRITE		1
#define UQFILT_SIGNAL		2
#define UQFILT_TIMER		3
#define UQFILT_PROC		4
#define UQFILT_AIO		5
#define UQFILT_VNODE		6
#define UQFILT_EXTENDMIN	64
#define UQFILT_EXTENDMAX	127

/* TIMER filter flags */
#define QNOTE_SECONDS		1
#define QNOTE_MSECONDS		2
#define QNOTE_USECONDS		3
#define QNOTE_NSECONDS		4

/* PROC filter flags */
#define QNOTE_EXIT		0x00000001

/* VNODE filter flags */
#define QNOTE_DELETE		0x01
#define QNOTE_WRITE		0x02
#define QNOTE_ATTRIB		0x04
#define QNOTE_RENAME		0x08

/* EXTEND filter flags */
#define QNOTE_FFCNTLMASK	0xFF000000
#define QNOTE_FFCOPY		0x01000000
#define QNOTE_FFOR		0x02000000
#define QNOTE_FFAND		0x04000000
#define QNOTE_FFANDOR		0x08000000
#define QNOTE_TRIGGER		0x80000000
#define QNOTE_FFLAGSMASK	(~QNOTE_FFCNTLMASK)

struct uevent {
	uintptr_t	ident;
	short		filter;
	unsigned short	flags;
	unsigned int	fflags;
	uintptr_t	data;
	void*		udata;
};


#define UQ_SET(ev, id, fi, fl, ff, da, ud)		\
do {	(ev)->ident  = (uintptr_t)(id);			\
	(ev)->filter = (short)(fi);			\
	(ev)->flags  = (unsigned short)(fl);		\
	(ev)->fflags = (unsigned int)(ff);		\
	(ev)->data   = (uintptr_t)(da);			\
	(ev)->udata  = (void*)(long)(ud);		\
} while (0)

extern int uqueue(void);

extern int uevent(int queue, const struct uevent* changes, size_t nchanges,
                  struct uevent* events, size_t nevents,
                  const struct timespec* timeout);

extern const char* uq_license;

/* -------------------------------------------------------------------------- */

#define UQ_SIGNO_AIO	SIGPOLL

extern int uq_aio_read(int, struct aiocb*);
extern int uq_aio_write(int, struct aiocb*);
extern int uq_aio_fsync(int, int, struct aiocb*);
extern int uq_aio_error(const struct aiocb*);
extern int uq_aio_return(struct aiocb*);
extern int uq_aio_cancel(int, struct aiocb*);
extern int uq_lio_listio(int, struct aiocb* const*, int, struct sigevent*);

/* ========================================================================== */
#endif /* ! UQUEUE_H */
