/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <errno.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

static int uf_proc_change(const struct uevent* ue)
{
	if ((0 != ue->data) || (QNOTE_EXIT != ue->fflags)) {
		errno = EINVAL;
		return -1;
	}
	return 0;
}


static const struct ufilter* cfilt;

static int uf_proc_notify(struct unote* un, const struct uevent* ue)
{
	return uf_notify(cfilt, un, ue);
}

static int uf_proc_copyout(struct uevent* ue, struct unote* un)
{
	ue->data   = un->ndata;
	ue->fflags = un->nfflags;

	un->nfflags = 0;
	un->ndata   = 0;

	return 0;
}

/* -------------------------------------------------------------------------- */

static void uf_proc_exit(void)
{
	if (0 != cfilt) {
		uf_exit(cfilt);
		cfilt = 0;
	}
}

extern const struct ufilter uf_proc_sigchld;

static int uf_proc_init(void)
{
	if (0 == cfilt) {
		if (0 == uf_init(&uf_proc_sigchld)) {
			cfilt = &uf_proc_sigchld;
		} else {
			return -1;
		}
	}

	return 0;
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_proc = {
	.init    = uf_proc_init,
	.exit    = uf_proc_exit,
	.change  = uf_proc_change,
	.notify  = uf_proc_notify,
	.copyout = uf_proc_copyout
};

/* ========================================================================== */
