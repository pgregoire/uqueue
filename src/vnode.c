/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/stat.h>

#include <errno.h>
#include <unistd.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

static int ffisvalid(int ff)
{
#define VFF	(QNOTE_DELETE | QNOTE_WRITE | QNOTE_ATTRIB | QNOTE_RENAME)

	return (0 == (~VFF & ff));
}
static int ffhasvalid(const struct uevent* ue)
{
	if (0 != (UQ_ADD & ue->flags)) {
		if (0 == ue->fflags) {
			return 0;
		}
	}

	return ffisvalid(ue->fflags);
}


static const struct ufilter* cfilt;

static int uf_vnode_change(const struct uevent* ue)
{
	struct stat st;

	if (0 == cfilt) {
		errno = ENOTSUP;
		return -1;
	}

	if (0 == ffhasvalid(ue)) {
		errno = EINVAL;
		return -1;
	}

	return fstat(ue->ident, &st);
}

static int uf_vnode_attach(struct unote* un, const struct uevent* ue)
{
	return uf_attach(cfilt, un, ue);
}

static int uf_vnode_detach(const struct unote* un)
{
	return uf_detach(cfilt, un);
}

static int uf_vnode_notify(struct unote* un, const struct uevent* ue)
{
	return uf_notify(cfilt, un, ue);
}

static int uf_vnode_copyout(struct uevent* ue, struct unote* un)
{
	return uf_copyout(cfilt, ue, un);
}

static void uf_vnode_exit(void)
{
	uf_exit(cfilt);
	cfilt = 0;
}


extern const struct ufilter uf_vnode_inotify;

static int uf_vnode_init(void)
{
	if (0 == cfilt) {
		if (0 == uf_init(&uf_vnode_inotify)) {
			cfilt = &uf_vnode_inotify;
		}
		/* The VNODE filter is not guaranteed */
	}

	return 0;
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_vnode = {
	.init    = uf_vnode_init,
	.exit    = uf_vnode_exit,
	.change  = uf_vnode_change,
	.attach  = uf_vnode_attach,
	.detach  = uf_vnode_detach,
	.notify  = uf_vnode_notify,
	.copyout = uf_vnode_copyout
};

/* ========================================================================== */
