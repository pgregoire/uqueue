/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <fcntl.h>
#include <unistd.h>

#include "uqueue-priv.h"

/* -------------------------------------------------------------------------- */

#define BILLION		1000000000ULL

uint64_t ts2u64(const struct timespec* ts)
{
	if (0 != ts) {
		return ((uint64_t)ts->tv_sec * BILLION) + ts->tv_nsec;
	}
	return -1;
}

uint64_t now(void)
{
	struct timespec ts;

	(void)clock_gettime(CLOCK_MONOTONIC, &ts);

	return ts2u64(&ts);
}

/* -------------------------------------------------------------------------- */

static int __nsleep(const struct timespec* ts)
{
	return clock_nanosleep(CLOCK_MONOTONIC, 0, ts, 0);
}

int nsleep(const struct timespec* to, uint64_t rt)
{
	struct timespec ts;

	if (0 != to) {
		return __nsleep(to);
	}
	if (0 == rt) {
		return 0;
	}

	ts.tv_sec  = (rt / BILLION);
	ts.tv_nsec = (rt % BILLION);

	return __nsleep(&ts);
}

/* -------------------------------------------------------------------------- */

int setup_chain(short fl, uintptr_t id, short ud, int add)
{
	struct uevent ue;

	add  = (0 != add) ? (UQ_ADD | UQ_ENABLE) : UQ_DELETE;
	add |= UQ_CHAIN;

	UQ_SET(&ue, id, fl, add, 0, 0, ud);

	return uchange(0, &ue);
}

int setnonblock(int fd)
{
	int f;

	if (0 > (f = fcntl(fd, F_GETFL))) {
		return -1;
	}

	f |= O_NONBLOCK;

	return fcntl(fd, F_SETFL, f);
}


static int setpip(int add, int* buf)
{
	if (0 != add) {
		if (0 != pipe(buf)) {
			return -1;
		}
		if (0 != setnonblock(buf[0])) {
			return -1;
		}
		return setnonblock(buf[1]);
	}

	(void)close(buf[0]);
	(void)close(buf[1]);
	buf[0] = -1;
	buf[1] = -1;

	return 0;
}

int setup_pipe(short fl, int* buf, int add)
{
	if (0 != add) {
		if (0 != setpip(1, buf)) {
			return -1;
		}
		if (0 != setup_chain(UQFILT_READ, buf[0], fl, 1)) {
			(void)setpip(0, buf);
			return -1;
		}
	} else {
		(void)setup_chain(UQFILT_READ, buf[0], fl, 0);
		return setpip(0, buf);
	}

	return 0;
}

/* ========================================================================== */
