/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <signal.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

static int uf_proc_sigchld_notify(struct unote* un, const struct uevent* ue)
{
	siginfo_t* s;
	struct uevent n;

	if ((UQFILT_SIGNAL == un->ue.filter) && (SIGCHLD == un->ue.ident)) {
		s        = (siginfo_t*)ue->data;
		n.filter = UQFILT_PROC;
		n.ident  = s->si_pid;
		n.data   = s->si_status;
		n.fflags = QNOTE_EXIT;

		(void)unotify(&n);

		return 0;
	}

	return 1;
}

/* -------------------------------------------------------------------------- */

static int uf_proc_sigchld_init(void)
{
	return setup_chain(UQFILT_SIGNAL, SIGCHLD, UQFILT_PROC, 1);
}

static void uf_proc_sigchld_exit(void)
{
	(void)setup_chain(UQFILT_SIGNAL, SIGCHLD, UQFILT_PROC, 0);
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_proc_sigchld = {
	.init   = uf_proc_sigchld_init,
	.exit   = uf_proc_sigchld_exit,
	.notify = uf_proc_sigchld_notify
};

/* ========================================================================== */
