/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

#ifndef UQUEUE_PRIV_H
#define UQUEUE_PRIV_H
/* ========================================================================== */

#include <pthread.h>

#include <config.h>
#include "uqueue.h"
#include "uqueue-queue.h"

/* -------------------------------------------------------------------------- */

struct unote;

struct uqueue {
	SLIST_HEAD(, unote)	lhead;
	TAILQ_HEAD(, unote)	qhead;

	pthread_mutex_t		lock;
	int			fd;
};

struct unote {
	struct uqueue*		uq;
	uintptr_t		ndata;
	uint32_t		nfflags;

	struct uevent		ue;
	SLIST_ENTRY(unote)	sle;
	TAILQ_ENTRY(unote)	tqe;
};

#define SF_FILTER	0x01
#define SF_IDENT	0x02
struct uscan {
	uintptr_t	ident;
	short		filter;
	short		sflags;
	int		(*eval)(struct unote*, void*);
	void*		earg;
};

/* control module */
extern int uchange(struct uqueue*, const struct uevent*);
extern int unotify(const struct uevent*);
extern int uscan(const struct uscan*);
extern struct unote* ufind(short, uintptr_t);

/* filtering module */
extern int  init_filters(void);
extern void exit_filters(void);
extern int  fchange(const struct uevent*);
extern int  fattach(struct unote*, const struct uevent*);
extern int  fdetach(const struct unote*);
extern int  fpoll(struct uevent*, size_t, uint64_t);
extern int  fnotify(struct unote*, const struct uevent*);
extern int  fcopyout(struct uevent*, struct unote*);

/* misc. */
#define NARRAY(a)		(sizeof(a) / sizeof((a)[0]))

extern uint64_t now(void);
extern uint64_t ts2u64(const struct timespec*);
extern int      setup_pipe(short, int*, int);
extern int      setup_chain(short, uintptr_t, short, int);
extern int      nsleep(const struct timespec*, uint64_t);
extern int	setnonblock(int);

/* -------------------------------------------------------------------------- */

#define DSO_EXPORT(decl)	decl __attribute__ ((visibility ("default")))

/* ========================================================================== */
#endif /* ! UQUEUE_PRIV_H */
