/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/*
 * Linux is unsuited to implement the VNODE filter. By tracking vnodes by name
 * instead of descriptors, inotify prevents us from safely tracking files across
 * rename operations. Also, acquiring the name of the vnode based on a
 * descriptor requires the use of readlink(2) on /proc; which can, legally, be
 * unmounted at run-time.
 *
 * Although inotify is semantically correct, accepting descriptors instead of
 * names would have allowed applications to choose whether to drop a "watch" if
 * the vnode's location change. By not providing the common F_GETPATH fcntl(2),
 * Linux creates a run-time dependency on /proc; which is simply brain-dead.
 * Also, note that readlink() does not indicate truncation.
 */

/* XXX
 *
 * - IN_DELETE_SELF is not triggered if the file is still opened.
 */

/* ========================================================================== */

#include "filters.h"

#ifdef USE_INOTIFY
#include <sys/inotify.h>
#include <sys/stat.h>

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <unistd.h>

#define _RBUFLEN	4096

/* -------------------------------------------------------------------------- */

static unsigned int convert_mask(uint32_t mask, struct unote* un)
{
	struct stat st;
	unsigned int c;

	c = 0;
	if (0 != (IN_DELETE_SELF & mask)) {
		c |= QNOTE_DELETE;
	}
	if (0 != ((IN_MODIFY | IN_CREATE) & mask)) {
		c |= QNOTE_WRITE;
	}
	if (0 != (IN_ATTRIB & mask)) {
		if (0 != fstat(un->ue.ident, &st)) {
			un->ue.flags |= UQ_EOF;
			return 0;
		}
		if ( (0 != (QNOTE_DELETE & un->ue.fflags)) &&
		     (0 == st.st_nlink)
		) {
			c |= QNOTE_DELETE;
		}
		c |= QNOTE_ATTRIB;
	}
	if (0 != (IN_MOVE_SELF & mask)) {
		c |= QNOTE_RENAME;
	}

	return c;
}

static uint32_t convert_fflags(unsigned int ff)
{
	uint32_t c;

	c = 0;
	if (0 != (QNOTE_DELETE & ff)) {
		c |= (IN_ATTRIB | IN_DELETE_SELF);
	}
	if (0 != (QNOTE_WRITE & ff)) {
		c |= (IN_MODIFY | IN_CREATE);
	}
	if (0 != (QNOTE_ATTRIB & ff)) {
		c |= IN_ATTRIB;
	}
	if (0 != (QNOTE_RENAME & ff)) {
		c |= IN_MOVE_SELF;
	}

	return c;
}

/* -------------------------------------------------------------------------- */

static int getfname(int fd, char* pb, size_t sz)
{
	int n;
	char b[PATH_MAX];

	snprintf(b, sizeof(b), "/proc/self/fd/%i", fd);

	if (0 >= (n = readlink(b, pb, sz))) {
		if (ENOENT == errno) {
			errno = EBADF;
		}
		return -1;
	}

	n = (sz == n) ? (n - 1) : n;

	pb[n] = '\0';

	return 0;
}

/* -------------------------------------------------------------------------- */

static int ifd = -1;

static int uf_vnode_inotify_attach(struct unote* un, const struct uevent* ue)
{
	int wd;
	char b[PATH_MAX];


	if (0 > getfname(ue->ident, b, sizeof(b))) {
		return -1;
	}

	if (0 > (wd = inotify_add_watch(ifd, b, convert_fflags(ue->fflags)))) {
		return -1;
	}

	un->ndata    = wd;
	un->nfflags |= QNOTE_TRIGGER;	/* mark as being monitored */

	return 0;
}

static int uf_vnode_inotify_detach(const struct unote* un)
{
	if (0 != (QNOTE_TRIGGER & un->nfflags)) {
		return inotify_rm_watch(ifd, un->ndata);
	}

	return 0;
}

static int seval(struct unote* un, void* _ie)
{
	struct uevent ue;
	struct inotify_event* ie;

	ie = (struct inotify_event*)_ie;

	if (IN_IGNORED == ie->mask) {
		return 0;
	}

	if (0 != (QNOTE_TRIGGER & un->nfflags)) {
		if (ie->wd == un->ndata) {
			ue.filter = UQFILT_VNODE;
			ue.ident  = un->ue.ident;
			ue.fflags = convert_mask(ie->mask, un);

			return (0 == unotify(&ue)) ? 1 : -1;
		}
	}

	return 0;
}

static int notify(const struct inotify_event* ie)
{
	struct uscan us;

	us.filter = UQFILT_VNODE;
	us.sflags = SF_FILTER;
	us.eval   = seval;
	us.earg   = (void*)ie;

	return uscan(&us);
}

static int read_ifd(int fd)
{
	int i;
	int n;
	char b[_RBUFLEN];
	struct inotify_event* p;

	do {
		if (0 >= (n = read(ifd, b, sizeof(b)))) {
			break;
		}

		p = (struct inotify_event*)b;
		for (i = 0 ; n > i ; i += (sizeof(*p) + p->len)) {
			p = (struct inotify_event*)&b[i];
			(void)notify(p);
		}
	} while (1);

	return 0;
}

static int uf_vnode_inotify_notify(struct unote* un, const struct uevent* ue)
{
	if (UQFILT_READ == un->ue.filter) {
		return read_ifd(ifd);
	}

	un->nfflags |= (ue->fflags & un->ue.fflags);

	if (0 != (QNOTE_RENAME & ue->fflags)) {
		(void)uf_vnode_inotify_detach(un);
		if (0 > uf_vnode_inotify_attach(un, &un->ue)) {
			un->ue.fflags |= UQ_EOF;
			return 1;
		}
	}

	return (0 != un->nfflags);
}

static int uf_vnode_inotify_copyout(struct uevent* ue, struct unote* un)
{
	ue->fflags  = un->nfflags;
	un->nfflags = QNOTE_TRIGGER;
	ue->fflags &= ~QNOTE_TRIGGER;

	return 0;
}

/* -------------------------------------------------------------------------- */

static void uf_vnode_inotify_exit(void)
{
	if (0 <= ifd) {
		(void)setup_chain(UQFILT_READ, ifd, UQFILT_VNODE, 0);
		(void)close(ifd);
		ifd = -1;
	}
}

static int uf_vnode_inotify_init(void)
{
	if (0 >= ifd) {
		if (0 > (ifd = inotify_init())) {
			return -1;
		}
		if (0 > setnonblock(ifd)) {
			uf_vnode_inotify_exit();
			return -1;
		}
		if (0 != setup_chain(UQFILT_READ, ifd, UQFILT_VNODE, 1)) {
			uf_vnode_inotify_exit();
			return -1;
		}
	}

	return 0;
}

/* -------------------------------------------------------------------------- */
#endif /* USE_INOTIFY */

const struct ufilter uf_vnode_inotify =
#ifdef USE_INOTIFY
{
	.init    = uf_vnode_inotify_init,
	.exit    = uf_vnode_inotify_exit,
	.attach  = uf_vnode_inotify_attach,
	.detach  = uf_vnode_inotify_detach,
	.notify  = uf_vnode_inotify_notify,
	.copyout = uf_vnode_inotify_copyout
};
#else /* ! USE_INOTIFY */
UF_DUMMY_INITIALIZER;
#endif /* USE_INOTIFY */

/* ========================================================================== */
