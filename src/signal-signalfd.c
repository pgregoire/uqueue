/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include "filters.h"

#ifdef USE_SIGNALFD
#include <sys/signalfd.h>

#include <errno.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

/* -------------------------------------------------------------------------- */

static int sfd = -1;
static sigset_t	sset;

/* -------------------------------------------------------------------------- */

static int __atde(int add, int sn, sigset_t* ss)
{
	if (0 != add) {
		return sigaddset(ss, sn);
	}
	return sigdelset(ss, sn);
}

static int atde(int add, int signo)
{
	sigset_t os;
	sigset_t ss;

	(void)memcpy(&ss, &sset, sizeof(ss));

	if (0 != __atde(add, signo, &ss)) {
		return -1;
	}

	if (0 != sigprocmask(SIG_BLOCK, &ss, &os)) {
		return -1;
	}

	if (sfd != signalfd(sfd, &ss, 0)) {
		(void)sigprocmask(SIG_SETMASK, &os, 0);
		return -1;
	}

	(void)__atde(add, signo, &sset);

	return 0;
}

static int uf_signal_signalfd_attach(struct unote* un, const struct uevent* ue)
{
	return atde(1, ue->ident);
}

static int uf_signal_signalfd_detach(const struct unote* un)
{
	return atde(0, un->ue.ident);
}

/* -------------------------------------------------------------------------- */

static void notify(siginfo_t* si)
{
	struct uevent ue;

	ue.filter = UQFILT_SIGNAL;
	ue.ident  = si->si_signo;
	ue.data   = (uintptr_t)si;

	(void)unotify(&ue);
}

/*
 * The sigaction implementation of the SIGNAL module
 * calls waitpid() when SIGCHLD gets delivered. For
 * compatibility reasons, this implementation calls
 * it too.
 */
static void sigchld(void)
{
	siginfo_t i;

	while (0 == uf_signal_sigchld(&i)) {
		notify(&i);
	}
}

static void sifd_to_si(const struct signalfd_siginfo *fi, siginfo_t* si)
{
	si->si_signo  = fi->ssi_signo;
	si->si_code   = fi->ssi_code;
	si->si_errno  = fi->ssi_errno;
	si->si_pid    = fi->ssi_pid;
	si->si_uid    = fi->ssi_uid;
	si->si_addr   = (void*)(long)fi->ssi_addr;
	si->si_status = fi->ssi_status;
	si->si_band   = fi->ssi_band;

	si->si_value.sival_ptr = (void*)(long)fi->ssi_ptr;
}

/*
 * While it might appear that sending an event message
 * containing the address of the siginfo on the stack is
 * unsafe, unotify() is assumed not to return until the
 * message can safely be discarded; either because it was
 * handled by a monitoring module or rejected.
 */
static int read_sfd(int fd)
{
	int i;
	int c;
	siginfo_t si;
	struct signalfd_siginfo b[128];

	do {
		if (0 >= (c = read(fd, b, sizeof(b)))) {
			if (EAGAIN != errno) {
				return -1;
			}
			return 0;
		}

		c /= sizeof(b[0]);

		for (i = 0 ; c > i ; i++) {
			if (SIGCHLD == b[i].ssi_signo) {
				sigchld();
			} else {
				sifd_to_si(&b[i], &si);
				notify(&si);
			}
		}
	} while (1);

	return 0;
}

static int
uf_signal_signalfd_notify(struct unote* un, const struct uevent* ue)
{
	if ((UQFILT_READ == un->ue.filter) && (sfd == un->ue.ident)) {
		return read_sfd(sfd);
	}

	un->ndata++;

	return 1;
}

static int uf_signal_signalfd_copyout(struct uevent* ue, struct unote* un)
{
	ue->data  = un->ndata;
	un->ndata = 0;

	return 0;
}

/* -------------------------------------------------------------------------- */

static int uf_signal_signalfd_init(void)
{
	if (0 > sfd) {
		if (0 > (sfd = signalfd(-1, &sset, SFD_NONBLOCK))) {
			return -1;
		}
		if (0 != setup_chain(UQFILT_READ, sfd, UQFILT_SIGNAL, 1)) {
			(void)close(sfd);
			sfd = -1;
			return -1;
		}
	}

	return 0;
}

static void uf_signal_signalfd_exit(void)
{
	if (0 <= sfd) {
		(void)setup_chain(UQFILT_READ, sfd, UQFILT_SIGNAL, 0);
		(void)close(sfd);
		sfd = -1;
	}
}

/* -------------------------------------------------------------------------- */
#endif /* USE_SIGNALFD */

const struct ufilter uf_signal_signalfd =
#ifdef USE_SIGNALFD
{
	.init    = uf_signal_signalfd_init,
	.exit    = uf_signal_signalfd_exit,
	.attach  = uf_signal_signalfd_attach,
	.detach  = uf_signal_signalfd_detach,
	.notify  = uf_signal_signalfd_notify,
	.copyout = uf_signal_signalfd_copyout
};
#else /* ! USE_SIGNALFD */
UF_DUMMY_INITIALIZER;
#endif /* USE_SIGNALFD */

/* ========================================================================== */
