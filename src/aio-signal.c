/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <aio.h>
#include <errno.h>
#include <signal.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

static int is_aiosig(const struct uevent* ue)
{
	return ((UQFILT_SIGNAL == ue->filter) && (UQ_SIGNO_AIO == ue->ident));
}

static int signote(const struct uevent* ue)
{
	siginfo_t* s;
	struct uevent e;

	s = (siginfo_t*)ue->data;
	e.ident  = (intptr_t)s->si_value.sival_ptr;
	e.filter = UQFILT_AIO;

	return unotify(&e);
}

static int uf_aio_signal_notify(struct unote* un, const struct uevent* ue)
{
	int e;

	if (0 != is_aiosig(&un->ue)) {
		(void)signote(ue);
		return 0;
	}

	switch (e = aio_error((struct aiocb*)ue->ident)) {
	case EINPROGRESS:
		return 0;
	case ECANCELED:
		return -1;
	case 0:
		un->nfflags = QNOTE_TRIGGER;
		un->ndata   = aio_return((struct aiocb*)ue->ident);
		break;
	default:
		un->nfflags |= UQ_EOF;
		un->nfflags |= QNOTE_TRIGGER;
		un->ndata    = e;
		break;
	}

	return 1;
}

static int uf_aio_signal_copyout(struct uevent* ue, struct unote* un)
{
	if (0 != (UQ_EOF & un->nfflags)) {
		ue->flags |= UQ_EOF;
	}

	ue->data  = un->ndata;

	return 0;
}

/* -------------------------------------------------------------------------- */

static int uf_aio_signal_init(void)
{
	return setup_chain(UQFILT_SIGNAL, UQ_SIGNO_AIO, UQFILT_AIO, 1);
}

static void uf_aio_signal_exit(void)
{
	(void)setup_chain(UQFILT_SIGNAL, UQ_SIGNO_AIO, UQFILT_AIO, 0);
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_aio_signal = {
	.init    = uf_aio_signal_init,
	.exit    = uf_aio_signal_exit,
	.notify  = uf_aio_signal_notify,
	.copyout = uf_aio_signal_copyout
};

/* ========================================================================== */
