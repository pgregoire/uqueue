/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/select.h>

#include <errno.h>
#include <string.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

static int hfd = -1;
static fd_set rset;
static fd_set wset;

/* -------------------------------------------------------------------------- */

static int scanh(void)
{
	int i;

	for (i = hfd ; 0 <= i ; i--) {
		if ((0 != FD_ISSET(i, &rset)) || (0 != FD_ISSET(i, &wset))) {
			break;
		}
	}

	return i;
}

/* -------------------------------------------------------------------------- */

static int uf_rw_select_change(const struct uevent* ue)
{
	errno = EBADF;
	return -((0 > (int)ue->ident) || (FD_SETSIZE <= ue->ident));
}

static int atde(short fl, int fd, int ad)
{
	fd_set* s;

	s = (UQFILT_READ == fl) ? &rset : &wset;

	if (0 != ad) {
		FD_SET(fd, s);
		if (hfd < fd) {
			hfd = fd;
		}
	} else {
		FD_CLR(fd, s);
		if (hfd <= fd) {
			hfd = scanh();
		}
	}

	return 0;
}

static int uf_rw_select_attach(struct unote* un, const struct uevent* ue)
{
	return atde(ue->filter, ue->ident, 1);
}

static int uf_rw_select_detach(const struct unote* un)
{
	return atde(un->ue.filter, un->ue.ident, 0);
}

static int uf_rw_select_notify(struct unote* un, const struct uevent* ue)
{
	return 1;
}

/* -------------------------------------------------------------------------- */

static void cpy(struct uevent* ue, int fd, short fl)
{
	ue->ident  = fd;
	ue->filter = fl;
}

static int
stor(struct uevent* buf, size_t cnt, fd_set* rs, fd_set* ws, int nelems)
{
	int i;
	size_t j;

	i = 0;
	j = 0;
	for (/**/ ; (hfd >= i) && (cnt > j) && (0 != nelems) ; i++) {
		if (0 != FD_ISSET(i, rs)) {
			cpy(&buf[j++], i, UQFILT_READ);
			nelems--;
		}
		if (cnt <= j) {
			break;
		}
		if (0 != FD_ISSET(i, ws)) {
			cpy(&buf[j++], i, UQFILT_WRITE);
			nelems--;
		}
	}

	return (int)j;
}

static int uf_rw_select_poll(struct uevent* buf, size_t cnt, uint64_t to)
{
	int n;
	fd_set rs;
	fd_set ws;
	struct timeval tv;

	if (0 > hfd) {
		errno = ENOENT;
		return -1;
	}

	to         = (to / 1000);
	tv.tv_sec  = (to / 1000000);
	tv.tv_usec = (to % 1000000);
	(void)memcpy(&rs, &rset, sizeof(rs));
	(void)memcpy(&ws, &wset, sizeof(ws));

	if (0 > (n = select((1 + hfd), &rs, &ws, 0, (-1 == to) ? 0 : &tv))) {
		return -1;
	}

	return stor(buf, cnt, &rs, &ws, n);
}

/* -------------------------------------------------------------------------- */

static int reset(int rd)
{
	fd_set* s;

	s = (0 != rd) ? &rset : &wset;

	FD_ZERO(s);
	hfd = scanh();

	return 0;
}

static int uf_rw_select_init(void)
{
	return reset(1);
}
static void uf_rw_select_exit(void)
{
	(void)reset(1);
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_rw_select = {
	.init   = uf_rw_select_init,
	.exit   = uf_rw_select_exit,
	.change = uf_rw_select_change,
	.attach = uf_rw_select_attach,
	.detach = uf_rw_select_detach,
	.poll   = uf_rw_select_poll,
	.notify = uf_rw_select_notify
};

/* ========================================================================== */
