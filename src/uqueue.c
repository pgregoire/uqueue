/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <sys/socket.h>

#include <errno.h>
#include <poll.h>

#include "control.h"

/* -------------------------------------------------------------------------- */

static pthread_mutex_t	lock = PTHREAD_MUTEX_INITIALIZER;
static struct uqueue*	sysq;
static struct uqueue*	queues[16];

#define LOCK()		(void)pthread_mutex_lock(&lock)
#define UNLOCK()	(void)pthread_mutex_unlock(&lock)

/* -------------------------------------------------------------------------- */

static struct unote* __uscan(const struct uscan* us)
{
	int i;
	struct unote* n;

	if ( ((0 == us->sflags) && (0 == us->eval)) ||
	     (SF_IDENT == us->sflags) )
	{
		errno = EINVAL;
		return (struct unote*)-1;
	}

	if (0 != (n = qscan(sysq, us))) {
		return n;
	}

	qlock(sysq);
	for (i = 0 ; NARRAY(queues) > i ; i++) {
		if (0 != (n = qscan(queues[i], us))) {
			break;
		}
	}
	qunlock(sysq);

	return n;
}

int uscan(const struct uscan* us)
{
	struct unote* n;

	if (0 == (n = __uscan(us))) {
		errno = ENOENT;
		return -1;
	} else if (((struct unote*)-1) == n) {
		return -1;
	}

	return 0;
}

struct unote* ufind(short fl, uintptr_t id)
{
	struct uscan s;

	s.sflags = (SF_FILTER | SF_IDENT);
	s.filter = fl;
	s.ident  = id;
	s.eval   = 0;

	return __uscan(&s);
}

/* -------------------------------------------------------------------------- */

static int init_uqueue(void)
{
	static int init;

	if (0 == init) {
		if (0 == sysq) {
			if (0 == (sysq = qcreate())) {
				return -1;
			}
		}
		if (0 > init_filters()) {
			return -1;
		}

		init++;
	}

	return 0;
}

/*
 * XXX The descriptor may also exist in registrations. */
static void fdclean(int idx)
{
	int d;
	int i;

	d = queues[idx]->fd;

	for (i = 0 ; NARRAY(queues) > i ; i++) {
		if (idx != i) {
			if ((0 != queues[i]) && (d == queues[i]->fd)) {
				qdestroy(queues[i]);
				queues[i] = 0;
			}
		}
	}
}

static int __uqueue(int idx)
{
	if (0 > init_uqueue()) {
		return -1;
	}

	if (0 == (queues[idx] = qcreate())) {
		return -1;
	}

	if (0 <= (queues[idx]->fd = socket(AF_UNIX, SOCK_DGRAM, 0))) {
		fdclean(idx);
		return queues[idx]->fd;
	}

	qdestroy(queues[idx]);
	queues[idx] = 0;

	return -1;
}

DSO_EXPORT(int uqueue(void));
int uqueue(void)
{
	int i;

	LOCK();
	for (i = 0 ; NARRAY(queues) > i ; i++) {
		if (0 == queues[i]) {
			break;
		}
	}

	if (NARRAY(queues) == i) {
		i = -1;
		errno = ENOMEM;
		goto X_UNLOCK;
	}

	i = __uqueue(i);

X_UNLOCK:
	UNLOCK();
	return i;
}

/* -------------------------------------------------------------------------- */

static int __getqueue(int fd)
{
	int i;

	i = (0 > fd) ? NARRAY(queues) : 0;

	LOCK();
	for (/**/ ; NARRAY(queues) > i ; i++) {
		if ((0 != queues[i]) && (fd == queues[i]->fd)) {
			break;
		}
	}

	if (NARRAY(queues) == i) {
		i = -1;
		errno = EINVAL;
	}

	UNLOCK();
	return i;
}

/* returns 1 if closed, -1 on error, 0 if not closed */
static int __qcheck(const struct uqueue* uq)
{
	int r;
	struct pollfd p;

	p.fd     = uq->fd;
	p.events = POLLIN;

	if (1 == (r = poll(&p, 1, 0))) {
		return (POLLNVAL == p.revents) ? 1 : 0;
	}

	return r;
}

static int qcheck(int idx)
{
	int r;

	do {
		if (0 >= (r = __qcheck(queues[idx]))) {
			if ((0 == r) || (EINTR != errno)) {
				return r;
			}
		} else {
			break;
		}
	} while (1);


	qdestroy(queues[idx]);

	LOCK();
	queues[idx] = 0;
	UNLOCK();

	errno = EINVAL;
	return -1;
}

struct uqueue* getqueue(int uq)
{
	if (0 > (uq = __getqueue(uq))) {
		return 0;
	}

	if (0 > qcheck(uq))  {
		return 0;
	}

	return queues[uq];
}

struct uqueue* getsysq(void)
{
	return sysq;
}

/* ========================================================================== */
