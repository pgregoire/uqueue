/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include <errno.h>

#include "filters.h"

/* -------------------------------------------------------------------------- */

static uint64_t __getival(uint64_t d, int f)
{
	switch (f) {
	case QNOTE_SECONDS:  return (d * 1000000000);
	case 0:              /* fall through */
	case QNOTE_MSECONDS: return (d * 1000000);
	case QNOTE_USECONDS: return (d * 1000);
	case QNOTE_NSECONDS: return d;
	}

	return (uint64_t)-1;
}

uint64_t uf_timer_getival(const struct uevent* ue)
{
	if (0 != ue) {
		return __getival(ue->data, ue->fflags);
	}

	errno = EINVAL;
	return -1;
}

/* -------------------------------------------------------------------------- */

#define MAXVAL_SECS		(86400ULL * 1000000000ULL)

static int uf_timer_change(const struct uevent* ue)
{
	uint64_t v = uf_timer_getival(ue);

	if (((uint64_t)-1 == v) || (MAXVAL_SECS < v)) {
		errno = EINVAL;
		return -1;
	}

	return 0;
}

/* -------------------------------------------------------------------------- */

static const struct ufilter* cfilt;

static int uf_timer_attach(struct unote* un, const struct uevent* ue)
{
	return uf_attach(cfilt, un, ue);
}

static int uf_timer_detach(const struct unote* un)
{
	return uf_detach(cfilt, un);
}

static int uf_timer_poll(struct uevent* ue, size_t sz, uint64_t to)
{
	return uf_poll(cfilt, ue, sz, to);
}

static int uf_timer_notify(struct unote* un, const struct uevent* ue)
{
	return uf_notify(cfilt, un, ue);
}

static int uf_timer_copyout(struct uevent* ue, struct unote* un)
{
	return uf_copyout(cfilt, ue, un);
}

/* -------------------------------------------------------------------------- */

static void uf_timer_exit(void)
{
	if (0 != cfilt) {
		uf_exit(cfilt);
		cfilt = 0;
	}
}


extern const struct ufilter uf_timer_thread;
extern const struct ufilter uf_timer_timerfd;

static int uf_timer_init(void)
{
	if (0 == cfilt) {
		if (0 == uf_init(&uf_timer_timerfd)) {
			cfilt = &uf_timer_timerfd;
		} else if (0 == uf_init(&uf_timer_thread)) {
			cfilt = &uf_timer_thread;
		} else {
			return -1;
		}
	}
	return 0;
}

/* -------------------------------------------------------------------------- */

const struct ufilter uf_timer = {
	.init    = uf_timer_init,
	.exit    = uf_timer_exit,
	.change  = uf_timer_change,
	.attach  = uf_timer_attach,
	.detach  = uf_timer_detach,
	.poll    = uf_timer_poll,
	.notify  = uf_timer_notify,
	.copyout = uf_timer_copyout
};

/* ========================================================================== */
