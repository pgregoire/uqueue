/*
 * This file is part of uqueue.
 *
 * Copyright (c) 2016, Philippe Grégoire <pggl@openmailbox.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the uqueue project.
 */

/* ========================================================================== */

#include "filters.h"

#ifdef USE_TIMERFD
#include <sys/timerfd.h>

#include <errno.h>
#include <unistd.h>

/* -------------------------------------------------------------------------- */

static int tmrfd = -1;

/* -------------------------------------------------------------------------- */

static int uf_timer_timerfd_notify(struct unote* un, const struct uevent* ue)
{
	int t;

	if ((UQFILT_READ == un->ue.filter) && (tmrfd == un->ue.ident)) {
		while (0 < read(tmrfd, &t, sizeof(t))) {
			/**/;
		}
		return 0;
	}

	return 1;
}

static void uf_timer_timerfd_exit(void)
{
	(void)setup_chain(UQFILT_READ, tmrfd, UQFILT_TIMER, 0);
	(void)close(tmrfd);
	uf_timer_rbt_exit();
	tmrfd = -1;
}

static int setup_timerfd(void)
{
#define _TFD_FLAGS		(TFD_NONBLOCK | TFD_CLOEXEC)

	int e;
	struct itimerspec it;

	if (0 > (tmrfd = timerfd_create(CLOCK_MONOTONIC, _TFD_FLAGS))) {
		return -1;
	}

	it.it_value.tv_sec     = 0;
	it.it_value.tv_nsec    = (1000000000 / UQUEUE_HERTZ);
	it.it_interval.tv_sec  = 0;
	it.it_interval.tv_nsec = it.it_value.tv_nsec;

	if (0 == timerfd_settime(tmrfd, 0, &it, 0)) {
		return 0;
	}

	e = errno;
	(void)close(tmrfd);
	errno = e;

	return (tmrfd = -1);
}

static int uf_timer_timerfd_init(void)
{
	int e;

	if (0 <= tmrfd) {
		return 0;
	}

	if (0 > uf_timer_rbt_init()) {
		return -1;
	}
	if (0 > setup_timerfd()) {
		goto X_ERR;
	}
	if (0 > setup_chain(UQFILT_READ, tmrfd, UQFILT_TIMER, 1)) {
		goto X_ERR;
	}

	return 0;

X_ERR:
	e = errno;
	uf_timer_timerfd_exit();
	errno = e;

	return -1;
}

/* -------------------------------------------------------------------------- */
#endif /* USE_TIMERFD */

const struct ufilter uf_timer_timerfd =
#ifdef USE_TIMERFD
{
	.init    = uf_timer_timerfd_init,
	.exit    = uf_timer_timerfd_exit,
	.attach  = uf_timer_rbt_attach,
	.detach  = uf_timer_rbt_detach,
	.poll    = uf_timer_rbt_poll,
	.notify  = uf_timer_timerfd_notify,
	.copyout = uf_timer_rbt_copyout
};
#else /* ! USE_TIMERFD */
UF_DUMMY_INITIALIZER;
#endif /* USE_TIMERFD */

/* ========================================================================== */
