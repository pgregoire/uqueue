uqueue							       Philippe Grégoire


			    Building and installing


REQUIREMENTS

    The uqueue project uses NetBSD make to control the build of its various
    elements. When this document refers to the 'make' command, a compatible make
    version, such as bmake, is assumed.

    The library requires an ISO/IEC 9899:1999 (C99) -compliant compiler and is
    known to succesfully compile and work using:

        - GNU GCC 4.9.3 and 5.3.1;
        - LLVM clang 3.6.2;

    Note that versions of GCC < 4.9.0 are not supported, by default. Also, tcc,
    the Tiny C Compiler, is known to have issues compiling a functioning
    library. As a matter of fact, gdb is not able to trace such a library.


PREPARE THE BUILD

    First, execute the 'configure' script. We recommend launching it from an
    empty directory to keep the source tree clean. Assuming that the source is
    located at ~/uqueue:

        $ mkdir ~/uqueue.obj
        $ cd ~/uqueue.obj
        $ ~/uqueue/configure

    The '--help' argument may be given to 'configure' to list build parameters.


BUILDING THE LIBRARY

    Once the 'configure' script has been executed, the library can be built and
    installed by issuing:

        $ make
        # make install


BUILDING THE DOCUMENTATION

    Most of uqueue's documentation is composed of plain text files. This section
    presents the requirements required to build elements not falling in this
    category.

    The manual page is written in mdoc, a roff macro language, and requires a
    compatible viewer. It may be converted to other formats (including the
    legacy man) with mandoc.

    uqueue's design paper is formatted using LaTeX and uses a number of common
    packages. Refer to the document's directory for specific requirements. For
    your convenience, pre-compiled Postscript and PDF versions of the document
    are located under dist/.


RUNNING THE TESTS

    The library comes with an extensive set of programs design to verify that
    its behavior is correct and efficient. Tests are built along the library and
    may be executed by issuing 'make check'.


NOTE TO PACKAGERS

    Packagers are invited to learn more about uqueue's versioning scheme and its
    relation to backward compatibility by reading doc/devel/version.txt.
