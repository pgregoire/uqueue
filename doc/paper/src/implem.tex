%% ========================================================================== %%
%%
%% This file is part of uqueue.
%%
%% Copyright (c) 2016, Philippe Grégoire
%% All rights reserved.
%%
%% ========================================================================== %%
\section{Implementation}
\label{sec:implem}

uqueue's public API, listed in figure \ref{fig:papi}, slightly differs from
kqueue's. First, as the library is not a strict re-implementation of kqueue,
names have been mangled to indicate that there exist differences with it and,
at the same time, allow uqueue to use kqueue without conflicts. Functions'
signature, however, were left untouched. We also provided $aio_*()$ wrappers,
allowing applications to monitor AIO operations with uqueue. The library's
private API, listed in figure \ref{fig:iapi}, also inspired by kqueue, is
composed of two (2) special macro constants, three (3) data structures and
twelve (12) functions, and is explained in this section.

%% -------------------------------------------------------------------------- %%
\subsection{Pre-requisites}

\subsubsection{The unote structure}

Briefly, the unote structure represents a registration and is used for book
keeping, control and state tracking. It contains the identifier of the queue it
is attached to, the latest copy of the application-given uevent structure, as
well as a few fields reserved for filters: $nfflags$, $ndata$ and a union
$nstore$ capable of holding a 64-bit unsigned integer and a pointer.

How registrations are stored is subject to change and irrelevant for the
purpose of this paper.

\subsubsection{Chaining filters}

Filter chaining is a functionality meant to reduce external dependencies by
promoting code re-use. It allows a monitoring module to rely on another filter
to monitor an event source it makes use of. For example, the POSIX-compliant
implementation of the AIO filter relies on the SIGNAL filter to monitor the
reserved UQ\_SIGNO\_AIO signal. Now, instead of directly relying on a facility
specified by POSIX, it indirectly relies on the facility used by the SIGNAL
filter. In cases where a facility such as signalfd() is used, uqueue is able to
avoid interrupts-based facilities and, consequently, improve performances. We
call the chaining filter, the filter making use of another.

\subsubsection{User-defined events}

uqueue allows application to define events of their own. Whenever a filter
identifier is within the range $[$UQFILT\_EXTENDMIN, UQFILT\_EXTENDMAX$]$, the
library interprets it as referencing the EXTEND filter. This filter is loosely
inspired by the USER filter, available on FreeBSD systems, but is way more
simple and intuitive to use. Its implementation is defined in section
\ref{sec:notes}

%% -------------------------------------------------------------------------- %%
\subsection{Notification queues}

Calling uqueue() for the first time causes the library to initialize itself and
call init\_filters(). The filter interface module then takes care of
initializing filter modules which, at this stage, select an appropriate
monitoring module to use based, among other things, on the process's
configuration and the availability of facilities. Assuming a strictly
POSIX-compliant platform, the READ filter, for example, will decide on poll() or
select() based on the process's open file descriptors limit and the value of the
FD\_SETSIZE macro constant. If the constant is less than the limit, poll() will
be used. At this point, chaining filters may also register event sources with
uqueue. To chain filters, the restricted UQ\_CHAIN flags has been defined to
indicate that event's occurences should be handled by the filter specified in
the $udata$ field. More on this later.

Once all filters has been successfully initialized, a new notification queue is
created and initialized before returning a unique identifier to the application.

%% -------------------------------------------------------------------------- %%
\subsection{Registrations}

When the application calls uevent(), it may "attach" event sources to a
notification queue for monitoring. Requests are processed sequentially by
checking whether there are any restricted flags before calling uchange(). If the
flags are invalid or the call fails, the control module checks whether there is
space left in the return buffer. If there is, the request is copied onto
it before setting the UQ\_ERROR flag and storing the error number in the $data$
field. If there are no space left, the module immediately returns -1 to the
application. Once all change requests have been processed, the control module
check if there were any errors and, if there are, the number of errors placed
in the return buffer is returned to the application. Note that this behavior is
equivalent to kqueue's.

As noted above, the restricted uchange() function is called to apply a request.
The first thing uchange() does is confirm the absence of mutually-exclusive
flags. It then calls fchange() to validate the filter-specific flags. The
fchange() function allows the caller to check whether a filter accepts the
the given change request. The filter is responsible for approving the request
by checking the $fflags$, $data$ and, optionally, $flags$ field.

If the filter does not accept the request, an error is returned to the caller.
Otherwise, uchange() checks if the event source is already monitored by uqueue.
If a matching unote is found, it checks whether the application-provided queue
matches the entry's queue. This check prevents an application from modifying
events attached to a queue different than the target. When deleting an event
source, the unote is used to dequeue and detach the entry from the queue. When
adding an event source, the request is copied into a new unote structure which
then gets attached to the queue. Finally, with the exception of deletions, the
request and its matching entry are sent to the filter by calling fattach().

The fattach() function instructs the appropriate filter to monitor the given
event source based on the entry's unote structure and the change request. Giving
these two (2) objects allows the filter to determine how to correctly
reconfigure itself. For example, a filter may check if the change requires a
system call and with what arguments, or it may use data that it stored for
itself in the unote.

Note that, if uchange() is given a nil target queue and the request has the
UQ\_CHAIN flag set, the target queue is considered to be the filter chaining
queue. This allows filter modules to chain without knowledge of the actual
implementation.

%% -------------------------------------------------------------------------- %%
\subsection{Polling the filters}

As discussed in section \ref{sec:consid}, uqueue's ability to monitor events is
based on a combination of polling- and interrupts-based facilities, and differs
from kqueue, which has filters notifying the kqueue system whenever an event
occurs --- making it use interrupts-based facilities, essentially. The
complexity and performance cost of dealing with interrupts-based facilities, in
the context of a user-space implementation, and the obligation to consider the
usage of polling-based facilities, required an implementation based on the
polling model. In other words: to produce notifications, filters are polled.

Whenever the application desires to collect notifications, the control module
starts by calling fpoll(). This function asks filters to produce
notification messages describing the source, filter, and any other related
data, of the events they observed. Note that, with the exception of the READ
filter, which is polled last, fpoll() does not pass the user-given timeout to
filters. Having acquired event notification messages from filters, the control
module iterates over them, invoking unotify() on each iterations.

At this point, there are a couple of points to clarify. First, let's mention
that, for performance reasons, we require the READ filter to always be backed by
a polling-based facility. Having a guarantee that polling the filter leads to a
system poll, it makes it an obvious choice for blocking. Consequently, blocking
on the READ filter allows monitoring modules to make use of the self-pipe
technique to unblock the system call -- whether it is by catching a signal or by
running from another thread. And since only the READ filter is allowed to block
and filters may be backed by a polling-based facility, the user-given timeout is
not passed to the others.

%% -------------------------------------------------------------------------- %%
\subsection{Handling events}
\label{subsec:implem-handl}

unotify() handles event notification messages produced by filters and accepts
two (2) arguments: the target queue and the notification message. The target
queue may be empty if the caller is unaware of it and can occur with chaining
filters. Otherwise, it is assumed to be the queue identified by the application.

When called, the function searches for a unote matching the event message. If no
match is found, an error is returned. unotify(), then, compares the target queue
with the unote's queue. If the queues do not match, unotify() checks if the
UN\_QABLE flag is set. This restricted flag is set by filters to indicate that
this event source is polled for its current state and that, consequently, cannot
be placed on the queue if it won't immediately be returned to the application.
If this flag is set and queues do not match, the event is silently ignored. Note
that ignoring this message forces uqueue to re-observe the event at a later
moment and prevents it from returning incorrect notifications. unotify(), then,
calls fnotify() with the unote and the message.

fnotify() invokes the filter identified by the $filter$ field of the event
message. This call gives the filter the opportunity to evaluate, use or update
the notification data stored in the unote based on the event. For example, the
VNODE filter may check if the event is of any interest to the user based on the
unote's $fflags$. It might then update the $nfflags$ field to reflect the
event(s) that have been observed. Finally, the filter returns a value indicating
if the user should be notified of the event.

Based on the return value of fnotify(), unotify() keeps handling the event or
drops it. When the filter has indicated to notify the user, the unote's
UQ\_CHAIN flag is tested and, if clear, the unote is enqueued on the appropriate
queue. If the flag is set, unotify() copies the unote's $udata$ field to the
event message's $filter$ field and calls fnotify() again.

This time, due to the modified $filter$ field, fnotify() calls the filter who
registered the entry -- the chaining filter. The filter is then free to
interpret the message has he sees fit. In the case of the strictly
POSIX-compliant implementation of the AIO filter --- which receives a SIGNAL
event ---, the message is used to produce an event message and sent to
unotify().

%% -------------------------------------------------------------------------- %%
\subsection{Returning notifications}

Once all event messages have been handled, the control module starts dequeuing
unote's off of the queue. For each unote, the module copies its uevent copy
onto the return buffer and calls fcopyout() with the unote and copied uevent.

fcopyout() takes care of invoking the appropriate filter to instruct it to
copy filter data to the notification. The filter may also modify the data stored
in the unote, if necessary.

Upon return, the control module checks if the unote has one of the UQ\_DISPATCH
or UQ\_ONESHOT flags set. If set, the module sets the UQ\_DISABLE or UQ\_DELETE
flag, respectively, before invoking uchange().

Obviously, the control module stops dequeuing unote until the queue is empty or
until the return buffer is full. Once one of these conditions is true, the
module sanitizes the notifications queue by scanning for unote's with the
UQ\_QABLE flag set. If any of these is encountered, the unote is dequeued. This
last second check prevents uqueue from returning incorrect notifications. The
number of uevent structures placed in the buffer is then returned to the
application.

%% -------------------------------------------------------------------------- %%
\subsection{Constraints}

As mentionned in subsection \ref{subsec:implem-handl}, the proper execution of
filter chaining relies on the fact the fnotify() calls the filter indicated by
the $filter$ field of the message. If it did not, the chaining filter would
never receive notifications for events it monitors for. Additionally, unotify()
may be called by a chaining filter and requires it to be reentrant.

Many chaining filters require the event message produced by the SIGNAL filter to


% SIGNAL filter message: siginfo pointer in data
% SIGNAL filter interrupts: self-pipe (chaining)

It is important to mention that being a polling-based system, the control module
cannot return queued notifications before having polled the filters. If it did,
notifications may be incorrect. A concrete example would be the SIGNAL filter
which returns the number of occurences of a signal since the last notification
returned to the applicaton. If the enqueued notification is returned, new
occurences of new signals will not have been observed and, at the moment the
notification is returned, the count would be incorrect.

In the description of the processing steps, earlier in this section, we did not
mention what protections are implemented to prevent concurrent accesses on local
data. This omission was meant to (a) lighten the text and (b) given the
implementors a degree of freedom.

%% -------------------------------------------------------------------------- %%

\begin{figure}
\lstinputlisting{lst/papi.h}
\caption{Public API}\label{fig:papi}
\end{figure}

\begin{figure}
\lstinputlisting{lst/iapi.h}
\caption{Private API}\label{fig:iapi}
\end{figure}

%% ========================================================================== %%
