#define UQFILT_USERBASE
#define UQFILT_USERLAST

struct uevent {
	uintptr_t	ident;
	short 		filter;
	unsigned short	flags;
	unsigned int	fflags;
	uintptr_t	data;
	void*		udata;
};

UQ_SET(&uev, ident, filter, fflags, data, udata);

int uqueue(void);

int uevent(int uqueue,
           const struct uevent* changes,
           size_t nchanges, struct uevent* events,
           size_t nevents,
           const struct timespec* timeout);

#define UQ_SIGNO_AIO
int uq_aio_read(int uqueue, struct aiocb* cb);
int uq_aio_write(int uqueue, struct aiocb* cb);
int uq_aio_fsync(int uqueue, int op, struct aiocb* cb);
int uq_aio_error(const struct aiocb* cb);
int uq_aio_return(struct aiocb* cb);
int uq_aio_cancel(int fd, struct aiocb* cb);
