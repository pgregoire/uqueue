#define UQ_CHAIN
#define UQ_QABLE

struct uqueue;
struct unote;
struct uscan;

/* control module */
int uchange(struct uqueue*, const struct uevent*);
int unotify(const struct uevent*);
int uscan(const struct uscan*);
struct unote* ufind(short, uintptr_t);

/* filtering module */
int  init_filters(void);
void exit_filters(void);

int fchange(const struct uevent*);
int fattach(struct unote*, const struct uevent*);
int fdetach(const struct unote*);
int ffilter(const struct timespec*);
int fnotify(struct unote*, const struct uevent*);
int fcopyout(struct uevent*, struct unote*);
