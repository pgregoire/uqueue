%% ========================================================================== %%
%%
%% This file is part of uqueue.
%%
%% Copyright (c) 2016, Philippe Grégoire
%% All rights reserved.
%%
%% ========================================================================== %%
\section{Structure}
\label{sec:struct}

Having acquired a good understanding of the problem space, our first step was
direct towards satisfying our portability goal. We started by listing the
elements of the software that are more prone to change. We then
adopted the information-hiding principle, set forth by Parnas\cite{parnas}, to
obtain a highly-modifiable product. According to this principle, details of the
system likely to change independently should be hidden from other modules and
modules should only expose details that are unlikely to change. This principle,
rarely applied in practice, attempts to reduce the number of assumptions
programmers make about other modules and, in the end, reduce the cost of
changing a software by minimizing the impact of changes.

%% -------------------------------------------------------------------------- %%
\subsection{Top-level modules}

\subsubsection{The control module}

The control module is responsible for the proper handling of registrations and
notifications. It needs to be modified if there are changes related to the way
registrations and notifications are processed. It hides software design
decisions based on resource consumption and algorithmic efficiency. Its secrets
are the data structures and algorithms used to manage registrations and
notifications.

\subsubsection{The filtering module}

The filtering module hides event filters and platforms details. It is affected
by changes related to the supported filters and platforms. It provides an
interface through which a variety of filters can be implemented without
affecting other parts of the software. Its primary secrets are the filters
supported and their implementation. Its secondary secrets are the data
structures and algorithms used to manage filters.

%% -------------------------------------------------------------------------- %%
\subsection{Second-level modules}

\subsubsection{The controller module}

The controller module, a sub-module of the control module, is responsible for
the processing of registrations management request and notifications. It needs
to be modified for changes related to registrations and notifications
processing. It hides the algorithms used to process registration modification
requests and notifications.

\subsubsection{The data storage module}

The data storage module, a sub-module of the control module, is responsible
for the storage of registrations and notifications. It needs to be modified to
satisfy new resource consumption requirements. Its secret are the data
structures and algorithms used to manage registrations and notifications
storage.

\subsubsection{The filter modules}

The filter modules, sub-modules of the filtering module, implement filters.
Filters differ in capabilities and behavior and each module is, thus,
responsible for the validation of registration management requests, and the
monitoring and handling of events. They expose an interface allowing them to be
used without knowledge of the nature of the event they provide support for.
A module is modified when it needs to support new options, when its behavior
must be changed, or when it needs to be implemented by exploiting a new
facility.

\subsubsection{The filter interface module}

The filter interface module, a sub-module of the filtering module, is
responsible for making event filters available to the control module. It hides
what filters are supported by exposing a filter-independent interface. Its
primary secrets are the filters supported and how they are managed. Its
secondary secrets are the data structures and algorithms used to support
filters.

%% -------------------------------------------------------------------------- %%
\subsection{Third-level decomposition}

\subsubsection{The monitoring modules}

The monitoring modules, sub-modules of the filter modules, are specialized
programs responsible for the monitoring and handling of a set of events using a
particular facility. They hide the details of the facility they exploit by
exposing an abstracting interface. A monitoring module is implemented whenever
there is a new facility to support, and is modified whenever there is a change
to the facility they use. Their primary secret is the details of the facility
they use.

%% -------------------------------------------------------------------------- %%
\subsection{Notes on the decomposition}

From top to bottom, the decomposition tends to less and less fuzzy. In the
first decomposition, we tried to split the software based on the amount of
details required to implement a certain behavior. In this case, the control
module requires near to no knowledge of event filters, while everything related
to the platforms and event filters implementation is grouped together. This
separation is based on the assumption that the behavior of the library is less
subject to change than the filters it provide.

In the second decomposition, we further isolated functional behavior from
elements likely to change. The control module is, thus, composed of an
application logic module and a data management module. This decomposition allows
implementors to optimize data management without affecting logic, and
vice-versa. The filtering module, on the other hand, is composed of an
interfacing module and event filters implementations. This structure provides
support for a wide variety of independently developed event filters with
minimal changes.

The third decomposition comes from the need to decouple system facilities from
event filters. In our study of facilities, we noted that a filter may be
implemented in many different ways in order to satisfy system requirements and
constraints. To this end, the behavior of a filter was decoupled from its
implementation; allowing a filter module to select an implementation based on
the availability of a particular system facility.

%% ========================================================================== %%
